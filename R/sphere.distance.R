
####################################################################################################
#               sphere.distance
####################################################################################################
#   purpose:    calculate distance between two points on a the globe
#   args:       lat1    (numeric)   latitude of point 1 in deg
#               lng1    (numeric)   longitude of point 1 in deg
#               lat2    (numeric)   latitude of point 2 in deg
#               lng2    (numeric)   longitude of point 2 in deg
#               r       (numeric)   (optional) default is the Earth radius in km
#   returns:    distance in km 
#   author:     stephan.henne@empa.ch
#   version:    0.1-051122
####################################################################################################
sphere.distance <- function(lat1, lng1, lat2, lng2, r = 6367){  # r:  Earth radius in km
    lat1 = lat1/180*pi
    lat2 = lat2/180*pi
    lng1 = lng1/180*pi
    lng2 = lng2/180*pi
    
    dd = 2*r*asin(sqrt((sin((lat1-lat2)/2))^2 + cos(lat1)*cos(lat2)*(sin((lng1-lng2)/2))^2 ))
    
    return(dd)
}


