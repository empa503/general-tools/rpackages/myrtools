
#   xlim and ylim should be the grid point centers of the outer grid points
#   
#   function last checked 2008-08-07, working well with various settings of pp

get.country.mask.shape = function(xlim, ylim, res=1, region="Switzerland", pp = 4, polylist){
    require(pixmap)
    require(myRplots)

    if (missing(xlim)) {
        xlim = c(-180+res, 180)
    }
    if (missing(ylim)){
        ylim = c(-90, 90)
    }
    width = round(diff(xlim)/res) + 1
    height = round(diff(ylim)/ res) + 1

    if(missing(polylist)){
        data(world.2008)
        world = world.2008
    } else {
        world = polylist
    }

#    msk = tolower(attr(world, "region.id")) %in% tolower(region)
    id = as.character(world[["NAME"]])
    idx = which(id %in% region)

    if (length(idx)==0) {
        warning(paste("Region", region, "not found."))
        return(NULL)
    } else {
        selected = world[idx,]
    }

    xlim = xlim + c(-1, 1)*res/2
    ylim = ylim + c(-1, 1)*res/2
    
    width= width*pp
    height = height*pp

    if (file.exists("map.pbm")) file.remove("map.pbm")        
    
    bitmap("map.pbm", "pbmraw", width=width/10, height=height/10, res=10)
    #X11(width=width/100, height=height/100)
    par(mar=c(0, 0, 0, 0), plt=c(0, 1, 0, 1))
    plot.new()
    plot.window(xlim, ylim, xaxs="i", yaxs="i")    
    plot(selected, col=1, xlim=c(-10, 50), ylim=c(30, 60), asp=NULL, add=TRUE)
#    try(map("worldHires", region, fill=1, add=TRUE, xlim=xlim, ylim=ylim, resolution=0), silent=TRUE)

    #rect(xlim[1], -90, xlim[2], -78, col=1)
    dev.off()
    repeat{
        Sys.sleep(1)
        if (file.exists("map.pbm")) break
    }
    cnm=list()
    tmp = read.pnm("map.pbm")
    cnm$zz = abs(tmp@grey - 1)
    file.remove("map.pbm")        

    cnm$xrng = xlim + c(1, -1)*res/(2*pp)
    cnm$yrng = ylim + c(1, -1)*res/(2*pp)

    if (pp>1){
        cnm = pool.grid(cnm, pp, mean)
    }

    cnm$source="shapefile"
    
    cnm$region = region
    
    return(cnm)    
}
