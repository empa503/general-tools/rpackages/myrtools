#' Bilinear interpolation from regular grid ot point(s)
#'
#' Interpolates from a 2D array to given point locations using bilinear interpolation. 
#' Note: Missing values (NA) in the 2D array are replaced by a nuermic value ('missval') prior
#'	to the interpolation. missval may need to be adjusted to fit the given problem. 
#'
#' @param grid object of type reg.grid containing the data to interpolate. Alternatively, 
#'				parameters 'z', 'xrng', 'yrng' can be given.
#' @param z		2D array containing the data from which to interpolate. Not required if 'grid' given.
#' @param xrng	Coordinate range in x direction. Not required if 'grid' given.
#' @param yrng	Coordinate range in y direction. Not required if 'grid' given.
#' @param x     x coordinate(s) onto which to interpolate. Have to be in same coordinate system as 
#'				grid data.
#' @param y     y coordinate(s) onto which to interpolate. Have to be in same coordinate system as 
#'				grid data.
#' @param missval  Numeric value assigned to cells with NA in 'z'. The interpolation uses
#'					external code that cannot deal with NA values. 
#' @param flip	If TRUE it is assumed that y dimension is flipp in 'z'. 
#'
#' @return Vector of interpolated values for each x,y pair.
#'
#' @export
bliint.points = function(grid, z, xrng, yrng, x, y, missval=-9999.9, flip=FALSE){
    #   dummy variables
    ansy1 = 0.0
    ansy2 = 0.0
    
    #   condition input parameters
    nn = min(c(length(x), length(y)))
    if (nn<1) stop("x and y have to contain at least one element")
    x = x[1:nn]
    y = y[1:nn]
    ansy = rep(0, nn)
    
	if (!missing(grid)){
		z = grid$zz
		xrng = grid$xrng
		yrng = grid$yrng
	}

    if (length(dim(z))!=2) stop("z has to be a matrix")
    if (flip) {
		z = z[nrow(z):1, ]
	} else {
		z = t(z)
	}
	nx = dim(z)[2]
	ny = dim(z)[1]
	
    z = as.vector(z)
	z[is.na(z)] = missval

    if (length(xrng)!=2 || mode(xrng)!="numeric" || any(is.na(xrng))) 
		stop("xrng has to be two element finite numerical vector")    
    if (length(yrng)!=2 || mode(xrng)!="numeric" || any(is.na(yrng))) 
		stop("yrng has to be two element finite numerical vector")
    
    
    #   bicubic interpolation
    res = .C("R_bliint_grid", as.double(z), as.integer(nx), as.integer(ny), 
        as.double(xrng[1]), as.double(xrng[2]), as.double(yrng[1]), as.double(yrng[2]),
        as.double(x), as.double(y), as.integer(nn), as.double(missval), 
		ansy=as.double(ansy), ansy1=as.double(ansy1), ansy2=as.double(ansy2))

    #   remove "Csingle" attribute of interpolated values
	res$ansy[res$ansy==as.double(missval)] = NA
    res = as.numeric(res$ansy)

    return(res)
}


bliint.points.internal = function(grid, x, y){
    nn = min(c(length(x), length(y)))
    if (nn<1) stop("x and y have to contain at least one element")
    x = x[1:nn]
    y = y[1:nn]

	dimz = dim(grid$zz)
	if (length(dimz)>3) {
		stop("zz can have a maximum of 3 dimensions")
	}	

	if (length(dimz)<2) {
		stop("zz can have a at least 2 dimensions")
	}	
	if (length(dimz)==2){
		dim(grid$zz) = c(dimz, 1)
	}
	nz = dim(grid$zz)[3]

	if (is.null(grid$nx)) grid$nx = dim(grid$zz)[1]
	if (is.null(grid$ny)) grid$ny = dim(grid$zz)[2]

	if (is.null(grid$dx)) grid$dx = diff(grid$xrng)/(grid$nx-1)
	if (is.null(grid$dy)) grid$dy = diff(grid$yrng)/(grid$ny-1)
	
	#	get lower left grid index 
	ix = trunc((x - grid$xrng[1])/grid$dx) + 1
	jy = trunc((y - grid$yrng[1])/grid$dy) + 1

	ix[ix<1 | ix>grid$nx] = NA	
	jy[jy<1 | jy>grid$ny] = NA	


    ddx = (x - (grid$xrng[1] + (ix-1)*grid$dx))/grid$dx
    ddy = (y - (grid$yrng[1] + (jy-1)*grid$dy))/grid$dy

	rddx = 1. - ddx
	rddy = 1. - ddy

	w1 = rddx * rddy
	w2 = ddx  * rddy
	w3 = rddx * ddy
	w4 = ddx  * ddy

	ix1 = ix 
	ix2 = ix+1
	ix3 = ix 
	ix4 = ix+1

	jy1 = jy 
	jy2 = jy
	jy3 = jy+1
	jy4 = jy+1

	
	if (nz == 1) {
		vals = grid$zz[cbind(ix1,jy1,1)]*w1 + grid$zz[cbind(ix2,jy2,1)]*w2 + 
		   	   grid$zz[cbind(ix3,jy3,1)]*w3 + grid$zz[cbind(ix4,jy4,1)]*w4
	} else {
		vals = sapply(1:nz, function(ii) {
				grid$zz[cbind(ix1,jy1,ii)]*w1 + grid$zz[cbind(ix2,jy2,ii)]*w2 + 
		   		grid$zz[cbind(ix3,jy3,ii)]*w3 + grid$zz[cbind(ix4,jy4,ii)]*w4
			})
		if (nn==1){
			dim(vals) = c(1,length(vals))
		}		
	}
	vals = t(vals)

	return(vals)
}




