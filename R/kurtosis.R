#	kurtosis: normal distribution will have kurtosis of 0
kurtosis = function(x, na.rm=FALSE){
	
	if (na.rm){
		n = length(which(!is.na(x)))
	} else {
		n = length(x)
	}
	
	m = mean(x, na.rm=na.rm)
	dd2 = (x-m)^2 
	
	kurt = n * sum(dd2^2, na.rm=na.rm)/sum(dd2, na.rm=na.rm)^2 - 3
	
	return(kurt)
}
#	example 

#x = rnorm(100)
#x = c(rep(30, 16), 50, 70, 90, 110)
#
#print(kurtosis(x))
