#   root mean square error
rmse = function(x, y, na.rm=FALSE){
    if (length(x)!=length(y)) {
        stop("x and y must have same length")        
    }
    if (na.rm) msk = which(!is.na(x) & !is.na(y))
	msk = 1:length(x)
    rmse = sqrt(sum((x-y)^2, na.rm=na.rm)/length(msk))
    return(rmse)
}

crmse = function(x, y, na.rm=FALSE){
    if (length(x)!=length(y)) {
        stop("x and y must have same length")        
    }
    if (na.rm) msk = which(!is.na(x) & !is.na(y))
	else msk = 1:length(x)
	crmse = sqrt(sum(((x[msk] - y[msk]) - mean(x[msk] - y[msk]))^2)/length(msk))

	return(crmse)
}

#	x: reference
#	y: test
bias = function(x, y, na.rm=FALSE){
    if (length(x)!=length(y)) {
        stop("x and y must have same length")        
    }
    if (na.rm) msk = which(!is.na(x) & !is.na(y))
	else msk = 1:length(x)
	bias = mean(y[msk] - x[msk])

	return(bias)
}

#	relation between rmse, crmse and bias: 
#		rmse = sqrt(bias^2 + crmse^2)

# Lin's concordance
# Lawrence, I. K. L.: A Concordance Correlation Coefficient to Evaluate Reproducibility, Biometrics, 45, 255-268, 10.2307/2532051, 1989.
concordance.cor = function(x, y, na.rm=TRUE){
    if (length(x)!=length(y)) {
        stop("x and y must have same length")        
    }
    if (na.rm) msk = which(!is.na(x) & !is.na(y))
	else msk = 1:length(x)

	r = cor(x[msk], y[msk])
	sd.x = sd(x[msk])
	sd.y = sd(y[msk])
	mn.x = mean(x[msk])
	mn.y = mean(y[msk])

	con = 2*r*sd.x*sd.y/(sd.x^2 + sd.y^2 + (mn.x-mn.y)^2)

	return(con)
}

