bcuint.points = function(z, xrng, yrng, x, y, flip=TRUE){
    #   dummy variables
    ansy1 = 0.0
    ansy2 = 0.0
    
    #   condition input parameters
    nn = min(c(length(x), length(y)))
    if (nn<1) stop("x and y have to contain at least one element")
    x = x[1:nn]
    y = y[1:nn]
    ansy = rep(0, nn)
    
    if (length(dim(z))!=2) stop("z has to be a matrix")
    if (flip){
		z = z[nrow(z):1, ]
	} else {
		z = t(z)
	}
	nx = dim(z)[2]
	ny = dim(z)[1]
	
	z = as.vector(z)

    if (length(xrng)!=2 || mode(xrng)!="numeric" || any(is.na(xrng))) 
		stop("xrng has to be two element finite numerical vector")    
    if (length(yrng)!=2 || mode(xrng)!="numeric" || any(is.na(yrng))) 
		stop("yrng has to be two element finite numerical vector")
    
    
    #   bicubic interpolation
    res = .C("R_bcuint_grid", as.single(z), as.integer(nx), as.integer(ny), 
        as.single(xrng[1]), as.single(xrng[2]), as.single(yrng[1]), as.single(yrng[2]),
        as.single(x), as.single(y), as.integer(nn), ansy=as.single(ansy), ansy1=as.single(ansy1), ansy2=as.single(ansy2))
    #   remove "Csingle" attribute of interpolated values
    res = as.numeric(res$ansy)

    return(res)
}
#
#require(myRplots)
#require(myRtools)
#zz = matrix(0, 15, 10)
#zz[5,5] = 10
#zz[3,3] = 8
#zz[4,4] = 7
#zz[2,2] = 10
#zz[1,1] = 5
#xrng = c(0,1.5)
#yrng = c(0,1)
#
#dx = 0.01
#dy = 0.01
#
#fill.grid(xrng, yrng, zz, zlim=c(-2,10), rotate=FALSE)
#xx = c(0, 1, 0.5)
#yy = c(0, 1, 0.5)
#val = bcuint.points(zz, xrng, yrng, x=xx, y=yy, flip=FALSE)
#points(xx,yy)
#
#print(cbind(xx,yy,val))
