"nine.point.filter" <-
function(cc, ucc, iterative=TRUE, max.it=30){
    verbose = FALSE
#    max.ch = 0.005 * mean(cc, na.rm=TRUE)
	if (!missing(ucc)){
	    max.ch = mean(ucc$upper-ucc$lower, na.rm=TRUE)/2
	} else {
		max.ch = mean(cc, na.rm=TRUE)
	}

    verbose=FALSE
    n.row = nrow(cc)
    n.col = ncol(cc)
    new.cc = cc
    if (verbose){
        print(range(cc, na.rm=TRUE))
        print(range(ucc$upper, na.rm=TRUE))
        print(range(ucc$lower, na.rm=TRUE))
    }
    
    weights = c(0.25, 0.5, 0.25, 0.5, 1, 0.5, 0.25, 0.5, 0.25)
    kk = 1
    if (verbose){
        X11(width=10, height=5)
        par(mfcol=c(1,2), mar=c(2,2,1,1)+0.1)
        fill.grid(cc, plot.key=FALSE)
        if (!missing(ucc)) fill.grid(ucc$lower, plot.key=FALSE)    
    }
    old.cc = cc
    repeat{
        cat("Horizontal filter:", kk, ". iteration\n")
        for (ii in 2:(n.row-1)){
            for (jj in 2:(n.col-1)){
                if (!is.na(old.cc[ii, jj])){
                    tmp = old.cc[(ii-1):(ii+1), (jj-1):(jj+1)]  
                    tmp = as.vector(tmp)
                    tmp = weighted.mean(tmp, weights, na.rm=TRUE)
                    if (missing(ucc)){
                        new.cc[ii, jj] = tmp
                    }else{
                        if (tmp>=ucc$lower[ii, jj] && tmp<=ucc$upper[ii, jj]) new.cc[ii, jj] = tmp
                        else new.cc[ii, jj] = old.cc[ii, jj]
                    }
                }
            }
        }
        diff = abs(new.cc - old.cc)
        
        if (verbose){
            X11(width=10, height=5)
            par(mfcol=c(1,2), mar=c(2,2,1,1)+0.1)
            fill.grid(new.cc, plot.key=FALSE)
            if (!missing(ucc)) fill.grid(diff, plot.key=FALSE)    
        }

        if (!iterative) break
        if (kk>max.it) break
		max.cur = max(diff, na.rm=TRUE)
        cat("Max. diff:", max.cur, "Mean change:", max.ch, "\n")
        if (max.cur < max.ch ) break
#        if (all(old.cc==new.cc)) break
        old.cc = new.cc
        kk = kk + 1
    }
    return(new.cc)  
}
