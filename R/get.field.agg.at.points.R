#' Does 4-d linear interpolation 
#'
#' Interpolates to a series of points (see 'x') from a set of 3D fields in time. 
#' The source fields are not provided directly but will be loaded if required by 
#' a designated function. 
#' Finally the interpolated values from all points are aggregated to a single value 
#' which is returned by the function. 
#' The function is using a global variable 'gctm' to store model fields!
#'
#' @param x is a data frame containing information on the locations to interpolate to
#'		it needs to have columns
#'			x:	longitude in degree east
#'			y:	latitude in degree north
#'			z:	altitude in m above model ground	
#'			dtm: time as chron object
#'
#' @param agg.FUN Aggregation function which is applied to all point interpolations. If set to 
#'			NULL, no aggregation is done.
#'	
#' @param gctm.FUN	function that takes a file name as input and returns a 3D geographic grid
#'				The latter has to contain fields xrng, yrng, dx, dy, zz (3D) and alt.agl (3D), 
#'					where zz holds the values to be interpolated from and alt.agl the grid 
#'					center altitudes in m above model ground.
#'
#' @param gctm.args futher arguments passed to gctm.FUN. Have to be given as named list. 
#' @param gctm.fls is a data.frame holding a list of available 3D model output files (column fn) and 
#'			the corresponding time (column dtm; centered on aggregation interval), the entries
#'			should be sorted by the time column (increasing)
#' @param max.fields	The function keeps a series of model fields in the memory to avoid multiple 
#'				reading of the same files. To keep the memory consumption at bay, a maximum
#'				number of stored fields is set. Default is 20. 
#' @param ... Further parameters passed to agg.FUN.
#'
#' @export
get.field.agg.at.points = function(x, plot.fn=NULL, agg.FUN=mean, gctm.FUN, gctm.args=NULL, 
	gctm.fls, max.fields = 50, ...){

	#	get index of gctm times just before particle time stamp
	x$gctm.fls = as.numeric(cut(x$dtm, gctm.fls$dtm))

	#	reduce to a unique list of gctm entries
	gctm.idx = sort(unique(x$gctm.fls))
	#	add next after each required file, to have upper temporal boundary
	gctm.idx = sort(unique(c(gctm.idx, gctm.idx+1)))

	#	get their file names
	gctm.fn = gctm.fls$fn[gctm.idx]
	gctm.dtm= gctm.fls$dtm[gctm.idx]

	#	check if global variable exists (if not create empty list else check that it is list)
	if (exists("gctm", envir=globalenv(), inherits=FALSE)){
		if (class(gctm)!="list") stop("global variable gctm is not a list. Please check.")
	} else {
		gctm <<- list()
	}

	#	check if new files need to be loaded
	for (ii in 1:length(gctm.idx)){
		#	load new file 
		if (! chron.2.string(gctm.dtm[ii], "%Y%m%d%H%M") %in% names(gctm)){
			tmp.args = c(list(fn=gctm.fn[ii], 
				dtm.out=gctm.dtm[ii]), gctm.args)
			#	store in global variable
			gctm[[chron.2.string(gctm.dtm[ii], "%Y%m%d%H%M")]] <<- do.call(gctm.FUN, tmp.args)
			
			#	make sure list does not get too long
			if (length(gctm) > max.fields){
				gctm[[1]] <<- NULL
				gc()
			}		
		}  
	}

	#	split points by time
	s.x = split(x, f=factor(x$gctm.fls))
	#	for each time 
	for (ii in 1:length(s.x)){
		#	calculate time interpolation parameters
		s.x[[ii]]$dt1 = as.numeric(s.x[[ii]]$dtm - gctm.fls$dtm[s.x[[ii]]$gctm.fls])
		s.x[[ii]]$dt2 = as.numeric(gctm.fls$dtm[s.x[[ii]]$gctm.fls+1] - s.x[[ii]]$dtm)
		s.x[[ii]]$ddt = s.x[[ii]]$dt1 + s.x[[ii]]$dt2

		#	spatial interpolation at bounding time steps
#		idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])])
		idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])], "%Y%m%d%H%M")
		s.x[[ii]]$c1 = bliint.points.3d(gctm[[idx]]$zz, xrng=gctm[[idx]]$xrng, 
			yrng=gctm[[idx]]$yrng, alt=gctm[[idx]]$alt.agl, x=s.x[[ii]]$x, y=s.x[[ii]]$y,
			z=s.x[[ii]]$z)
#		idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])+1])
		idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])+1], "%Y%m%d%H%M")
		s.x[[ii]]$c2 = bliint.points.3d(gctm[[idx]]$zz, xrng=gctm[[idx]]$xrng, 
			yrng=gctm[[idx]]$yrng, alt=gctm[[idx]]$alt.agl, x=s.x[[ii]]$x, y=s.x[[ii]]$y,
			z=s.x[[ii]]$z)

		#	final temporal interpolation
		s.x[[ii]]$c = (s.x[[ii]]$c1*s.x[[ii]]$dt2 + s.x[[ii]]$c2*s.x[[ii]]$dt1)/s.x[[ii]]$ddt

		#	plot model fields and particle positions
		if (!is.null(plot.fn)){

			fn = paste0(plot.fn, sprintf("_%03d", ii))

			if (is.null(gctm.args$xlim)) gctm.args$xlim = c(-25,30)
			if (is.null(gctm.args$ylim)) gctm.args$ylim = c( 35,60)
			if (is.null(gctm.args$zlim)) gctm.args$zlim = c(1850, 1950) 
			if (is.null(gctm.args$key.title)) gctm.args$key.title = "VMR (?)"

			open.graphics.device(fn, height=15, width=10, res=600, pointsize=9)

			layout(matrix(c(1:6,7,7), nrow=4, byrow=TRUE), heights=c(1,1,1, lcm(1.25)))
			par(mar=c(3,3,3,1)+.1, lwd=0.5)
		
			idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])])
			idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])], "%Y%m%d%H%M")

			#	mean model levels
			model.alt = apply(gctm[[idx]]$alt.agl, 3, mean)
			#	check where particles reside
			s.x[[ii]]$level = cut(s.x[[ii]]$z, breaks=model.alt)
			#	occupied levels
			oc.lev = which(summary(na.omit(s.x[[ii]]$level))>100)
			if (length(oc.lev)==0){
				oc.lev = which(summary(na.omit(s.x[[ii]]$level))>0)
			}
			lev.low = oc.lev[1]
			if (length(oc.lev)>2){
				lev.mid = oc.lev[round(length(oc.lev)/2.)]
				lev.high = oc.lev[length(oc.lev)]
			} else {
				lev.mid = oc.lev[2]
				lev.high = lev.mid + 1 
			}
	
			if (!is.na(lev.high)){
				idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])])
				idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])], "%Y%m%d%H%M")
				msk = which(as.numeric(s.x[[ii]]$level)==lev.high)
				fill.grid(gctm[[idx]], lev=lev.high, rotate=FALSE, xlim=gctm.args$xlim, 
					ylim=gctm.args$ylim, borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE,
					color.palette=yellow.blue.fade.colors)
				points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
				mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])]), 3, 1, 
					cex=par("cex"))
				mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
				mtext(paste("Level:", round(model.alt[lev.high]), "m"), 3, 0, cex=par("cex"))
				if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
		#
				idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])+1])
				idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])+1], "%Y%m%d%H%M")
				fill.grid(gctm[[idx]], lev=lev.high, rotate=FALSE, xlim=gctm.args$xlim, 
					ylim=gctm.args$ylim, borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE,
					color.palette=yellow.blue.fade.colors)
				points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
				mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])+1]), 3, 1, 
					cex=par("cex"))
				mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
				mtext(paste("Level:", round(model.alt[lev.high]), "m"), 3, 0, cex=par("cex"))
				if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
			} else {
				plot.new()
				plot.new()
			}
#			mtext(paste("#:", nrow(s.x[[ii]])), 3, 1, adj=-0.2)
	
			if (!is.na(lev.mid)){
				idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])])
				idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])], "%Y%m%d%H%M")
				msk = which(as.numeric(s.x[[ii]]$level)==lev.mid)
				fill.grid(gctm[[idx]], lev=lev.mid, rotate=FALSE, xlim=gctm.args$xlim, 
					ylim=gctm.args$ylim, borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE,
					color.palette=yellow.blue.fade.colors)
				points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
				mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])]), 3, 1, 
					cex=par("cex"))
				mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
				mtext(paste("Level:", round(model.alt[lev.mid]), "m"), 3, 0, cex=par("cex"))
				if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
		#
				idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])+1])
				idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])+1], "%Y%m%d%H%M")
				fill.grid(gctm[[idx]], lev=lev.mid, rotate=FALSE, xlim=gctm.args$xlim, 
					ylim=gctm.args$ylim, borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE, 
					color.palette=yellow.blue.fade.colors)
				points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
				mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])+1]), 3, 1, 
					cex=par("cex"))
				mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
				mtext(paste("Level:", round(model.alt[lev.mid]), "m"), 3, 0, cex=par("cex"))
				if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
			} else {
				plot.new()
				plot.new()
			}
	
	
			idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])])
			idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])], "%Y%m%d%H%M")
			msk = which(as.numeric(s.x[[ii]]$level)==lev.low)
			fill.grid(gctm[[idx]], lev=lev.low, rotate=FALSE, xlim=gctm.args$xlim, 
				ylim=gctm.args$ylim, borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE,
				color.palette=yellow.blue.fade.colors)
			points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
			mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])]), 3, 1, cex=par("cex"))
			mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
			mtext(paste("Level:", round(model.alt[lev.low]), "m"), 3, 0, cex=par("cex"))
			if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
	#
			idx = basename(gctm.fls$fn[as.numeric(names(s.x)[ii])+1])
			idx = chron.2.string(gctm.fls$dtm[as.numeric(names(s.x)[ii])+1], "%Y%m%d%H%M")
			colscale =fill.grid(gctm[[idx]], lev=lev.low, rotate=FALSE, 
				xlim=gctm.args$xlim, ylim=gctm.args$ylim, 
				borders=TRUE, zlim=gctm.args$zlim, plot.key=FALSE,
				color.palette=yellow.blue.fade.colors, key.pos=1, 
				key.title=gctm.args$key.title)
			points(s.x[[ii]]$x[msk], s.x[[ii]]$y[msk], pch=".", cex=1, col=2)
			mtext(paste("Model:", gctm.fls$dtm[as.numeric(names(s.x)[ii])+1]), 3, 1, cex=par("cex"))
			mtext(paste("Particles:", mean(s.x[[ii]]$dtm)), 3, 2, cex=par("cex"))
			mtext(paste("Level:", round(model.alt[lev.low]), "m"), 3, 0, cex=par("cex"))
			if (length(msk)>0) mtext(paste("#:", length(msk)), 3, -1, adj=0, cex=par("cex"))
	#
			plot.colorpalette(colscale)
	
			dev.off()
		}

	}
	if (!is.null(agg.FUN)){	
		if (!is.list(agg.FUN)) agg.FUN = list(agg.FUN)
		c = list()
		for (ii in 1:length(agg.FUN)){
			c[[ii]] = agg.FUN[[ii]](unlist(sapply(s.x, "[[", "c")), ...)
			if (any(is.na(c[[ii]]))) {
				cat("NA produced in interpolation\n")
			}
		}
	} else {
		c = unlist(sapply(s.x, "[[", "c"))
	}

	return(c)
}
