###############################################################################
#             days.in.month
###############################################################################
# purpose:    return the number of days in a month
# arguments:  x           a (vector) of chron or dates object(s)
# author:     joerg.klausen@empa.ch
# version:    v1.0-060223
###############################################################################
days.in.month = function(x) {
    if (!class(x)[1] %in%  c("chron", "dates", "times"))
        stop("x must be a dates or chron object")
    require(chron)
    mm = as.numeric(months(x))
    dd = ifelse (mm < 8, mm %% 2 + 30, (mm + 1) %% 2 + 30)
    dd[ mm == 2] = 28
    dd[ which(mm == 2 & leap.year(x)) ] = 29
    return(dd)
}

##	example 
#require(chron)
#
#dtm = chron("2001-05-01", "01:00:00", format=c("y-m-d", "h:m:s"))
#
#cat(format(dtm), days.in.month(dtm), "\n")