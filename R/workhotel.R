#############################################################################
#      Working-Hotelling confidence bands
#############################################################################
# purpose:    compute W-H confidence bands for multiple use of regression
# arguments:  x,y      (numeric)      vector of x,y values
#             ffitexy  (data frame)   output of fitexy
#             alpha    (numeric)      level of confidence
# author:     joerg.klausen@empa.ch
# version:    v1.0-060306
# changes: 
#############################################################################
#workhotel <- function(x,y,alpha){
#      # plots Working-Hotelling confidence bands for regression line
#      # and individual prediction intervals
#      # source: http://www.math.umt.edu/patterson/542/workhotel.SSC
#      if(length(x) != length(y)) stop("x and y must be same length")
#      reg <- lm(y~x)
#      n <- length(y)
#      mse <- sum(reg$residuals^2)/(n-2)
#      xval <- seq(min(x),max(x),length=500)
#      yhat <- reg$coef[1] + reg$coef[2]*xval
#      crit <- sqrt(2*qf((1-alpha),2,n-2))
#      critpred <- qt(1-alpha/2,n-2)
#      se.yhat <- sqrt(mse*(1/n+(xval-mean(x))^2/((n-1)*var(x))))
#      se.pred <- sqrt(mse*(1+1/n+(xval-mean(x))^2/((n-1)*var(x))))
#      umband <- yhat + crit*se.yhat
#      lmband <- yhat - crit*se.yhat
#      upband <- yhat + critpred*se.pred
#      lpband <- yhat - critpred*se.pred
#      ymin <- min(c(y,lpband))
#      ymax <- max(c(y,upband))
#      plot(x,y,ylim=c(ymin,ymax), main=paste(1-alpha," bands"))
#      abline(reg)
#      lines(xval,umband)
#      lines(xval,lmband)
#      lines(xval,upband,lty=2)
#      lines(xval,lpband,lty=2)
#      }
workhotel = function(x, y, ffitexy, alpha, xrng) {
    n = length(x)
    resid = y - (ffitexy$a + ffitexy$b*x)
    mse = sum(resid^2)/(n-2)
    sig = sqrt(mse)
    
    w = sqrt(2*qf(1-alpha,2,n-2))
    if (missing(xrng)){
		Xh = seq(min(x), max(x), length.out=100)
	} else {
		Xh = seq(xrng[1], xrng[2], length.out=100)
	}
    Yh = ffitexy$a + ffitexy$b*Xh
    # statistics for unweighted linear regression
    ### MUST CHECK IF THIS APPLIES! ####
    mux = mean(x)
	muy = mean(y)
    s2x = sum((x-mux)^2)/(n-1)
	s2y = sum((y-muy)^2)/(n-1)
    sxy = sum((x-mux)*(y-muy))/(n-1)
    syh = sig*sqrt(1/n+(Xh-mux)^2/(s2x*(n-1)))

    return(data.frame(cbind(X = Xh, lower = Yh-w*syh, upper = Yh + w*syh)))
}
