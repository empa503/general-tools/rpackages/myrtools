# TODO: Add comment
# 
# Author: hes134
###############################################################################
density.2.irr.grid = function(grid, b=5, combine=3, levels, sort=TRUE, flipped=TRUE, 
	remove.zero.only=FALSE, para="zz", digits=6, ...){
	
	if (length(combine)>1) {
		if(any(diff(combine)>0)) stop("combine has to be in descending order")
	} else {
		combine = 2^(combine:1)
	}
	
	grid[[para]][is.na(grid[[para]])] = 0 
	if (missing(levels)){
#        levels = quantile(grid$zz, seq(1/length(combine), 1, length.out=length(combine)))
		levels = b*quantile(grid[[para]][grid[[para]]>0], 0.95, na.rm=TRUE)/c(combine, 1)^2        
	}
	#cat("Levels:", levels[length(levels)]/3600, "(h) \n")
	
	if (flipped){
		nx = ncol(grid[[para]])
		ny = nrow(grid[[para]])
	} else {
		ny = ncol(grid[[para]])
		nx = nrow(grid[[para]])
	}
	dx = round(diff(grid$xrng)/(nx-1), digits)
	dy = round(diff(grid$yrng)/(ny-1), digits)
	
	#	extent grid so that nx is a multiple of max(combine)
	if (any(nx %% combine != 0) || any(ny %% combine != 0)) {
		
		add.nx = nx %% max(combine) 
		add.ny = ny %% max(combine)
		
		add.nx.left = trunc(add.nx/2)
		add.nx.right = ceiling(add.nx/2)
		
		add.ny.bottom = trunc(add.ny/2)
		add.ny.top = ceiling(add.ny/2)
	
		new.xrng = round(grid$xrng + c(-add.nx.left, add.nx.right)*dx, digits=6)
		new.yrng = round(grid$yrng + c(-add.ny.bottom, add.ny.top)*dy, digits=6)
		
		grid = extend.grid(grid, new.xrng, new.yrng, new.val=0, flipped=flipped, para=para)
		if (flipped){
			nx = ncol(grid[[para]])
			ny = nrow(grid[[para]])
		} else {
			ny = ncol(grid[[para]])
			nx = nrow(grid[[para]])
		}
		dx = round(diff(grid$xrng)/(nx-1), digits)
		dy = round(diff(grid$yrng)/(ny-1), digits)
	}
	
	if(flipped){
		covered = matrix(FALSE, nrow=ny, ncol=nx)
	} else {
		covered = matrix(FALSE, nrow=nx, ncol=ny)
	}
	x = NULL
	y = NULL
	ir.dx = NULL
	ir.dy = NULL
	z = NULL
	for (kk in 1:length(combine)){
		c.nx = nx / combine[kk]
		c.ny = ny / combine[kk]
		
		for (ii in 1:c.nx){
			for (jj in 1:c.ny){
				ix = rep((1+combine[kk]*(ii-1)):(combine[kk]*ii), each=combine[kk])
				jy = rep((1+combine[kk]*(jj-1)):(combine[kk]*jj), combine[kk])
				if (flipped){
					msk = cbind(jy, ix)
				} else {
					msk = cbind(ix,jy)
				}
				if (!all(covered[msk])){
					tmp = mean(grid[[para]][msk], na.rm=TRUE)
					#	add grid cell if density smaller level
					if (tmp<levels[kk+1]){
						covered[msk] = TRUE
						x = c(x, round(grid$xrng[1] + ((ii-0.5)*combine[kk]-0.5)*dx, digits))
						if (flipped){
							y = c(y, round(grid$yrng[2] - ((jj-0.5)*combine[kk]-0.5)*dy, digits))
						} else {
							y = c(y, round(grid$yrng[1] + ((jj-0.5)*combine[kk]-0.5)*dy, digits))
						}	
						ir.dx = c(ir.dx, dx*combine[kk])
						ir.dy = c(ir.dy, dy*combine[kk])
						z = c(z, tmp)
					} 
				}
			}
		}
	}

	
	#   remaining grid cells
	if (combine[length(combine)]==2){
		msk = which(!covered)
		if (flipped){
			tmp.x = rep(round(seq(grid$xrng[1], grid$xrng[2], dx), digits), each=ny)
			tmp.y = rep(round(seq(grid$yrng[2], grid$yrng[1], -dy), digits), nx)
		} else {
			tmp.x = rep(round(seq(grid$xrng[1], grid$xrng[2], dx), digits), ny)
			tmp.y = rep(round(seq(grid$yrng[1], grid$yrng[2], dy), digits), each=nx)
		}
		x = c(x, tmp.x[msk])
		y = c(y, tmp.y[msk])
		ir.dx = c(ir.dx, rep(dx, length(msk)))
		ir.dy = c(ir.dy, rep(dy, length(msk)))    
		z = c(z, grid[[para]][msk])
	} else {
		cmb = combine[length(combine)]/2
		
		c.nx = nx / cmb
		c.ny = ny / cmb
		
		for (ii in 1:c.nx){
			for (jj in 1:c.ny){
				ix = rep((1+cmb*(ii-1)):(cmb*ii), each=cmb)
				jy = rep((1+cmb*(jj-1)):(cmb*jj), cmb)
				if (flipped){
					msk = cbind(jy, ix)
				} else {
					msk = cbind(ix,jy)
				}
				if (!all(covered[msk])){
					tmp = mean(grid[[para]][msk], na.rm=TRUE)
					#	add grid cell if density smaller level
					covered[msk] = TRUE
					x = c(x, round(grid$xrng[1] + ((ii-0.5)*cmb-0.5)*dx, digits))
					if (flipped){
						y = c(y, round(grid$yrng[2] - ((jj-0.5)*cmb-0.5)*dy, digits))
					} else {
						y = c(y, round(grid$yrng[1] + ((jj-0.5)*cmb-0.5)*dy, digits))
					}	
					ir.dx = c(ir.dx, dx*cmb)
					ir.dy = c(ir.dy, dy*cmb)
					z = c(z, tmp)
				}
			}
		}
	} 

	#	construct grid object
	igrid = list(x=x, y=y, z=z, dx=ir.dx, dy=ir.dy)
	
	if (remove.zero.only){
		msk = which(igrid$z<=0)
	} else {
		msk = which(igrid$z<levels[1])
	}
	if (length(msk)>0){
		igrid$x = igrid$x[-msk]
		igrid$y = igrid$y[-msk]
		igrid$z = igrid$z[-msk]
		igrid$dx = igrid$dx[-msk]
		igrid$dy = igrid$dy[-msk]
	}   

	# grid range 
	igrid$xrng = range(c(igrid$x+igrid$dx/2, igrid$x-igrid$dx/2))
	igrid$yrng = range(c(igrid$y+igrid$dy/2, igrid$y-igrid$dy/2))
	
	if (sort){
		msk = order(igrid$z, decreasing=TRUE)
		igrid$x = igrid$x[msk]
		igrid$y = igrid$y[msk]
		igrid$z = igrid$z[msk]
		igrid$dx = igrid$dx[msk]
		igrid$dy = igrid$dy[msk]
	}
	
	#   derive sums from grid size information
	igrid$z = igrid$z*(igrid$dx/dx)*(igrid$dy/dy)
	
	class(igrid) = "irr.grid"
	
	return(igrid)
}

#load("W:\\2SMONG\\footprints\\total.footprint.main.CFC_11.SDZ_10_GSN_70.rda")
##	flipped version
#tot.ftpr$zz = tot.ftpr$conc
#irg.ft = density.2.irr.grid(tot.ftpr)
#plot(irg.ft)
#
##	non-flipped version
#tot.ftpr$zz = t(tot.ftpr$conc)
#for (ii in 1:ncol(tot.ftpr$conc)) tot.ftpr$zz[ii,]= rev(tot.ftpr$conc[,ii])
#irg.ft = density.2.irr.grid(tot.ftpr, flipped=FALSE)
#plot(irg.ft, log.scale=TRUE)
