#	alias for divide.grid
grid.refine = function(...){
	return(divide.grid(...))
}

#' @export
divide.grid = function(grid, para="zz", div=2, dens.cor=TRUE, flipped=FALSE){

	nn.dim = length(dim(grid[[para]]))
	if (nn.dim==3){
		nn.z = dim(grid[[para]])[3]
	} else {
		nn.z = 1
	}

	if (flipped){
		nx = dim(grid[[para]])[nn.dim]
		ny = dim(grid[[para]])[nn.dim-1]
	} else {
		nx = dim(grid[[para]])[1]
		ny = dim(grid[[para]])[2]
	}
	
	if (is.null(grid$dx)) grid$dx = diff(grid$xrng)/(nx-1)
	if (is.null(grid$dy)) grid$dy = diff(grid$yrng)/(ny-1)
	
	dx.new = grid$dx/div
	dy.new = grid$dy/div
	
	xrng.new = grid$xrng + c(-1,1)*(grid$dx/2-dx.new/2)
	yrng.new = grid$yrng + c(-1,1)*(grid$dy/2-dy.new/2)
	
	nx.new = round(diff(xrng.new)/dx.new) + 1
	ny.new = round(diff(yrng.new)/dy.new) + 1
	

	if (flipped){
		if (nn.dim==3){
			zz.new = array(NA, dim=c(nn.z, ny.new, nx.new))	
		} else {
			zz.new = array(NA, dim=c(ny.new, nx.new))	
		}
	} else {
		if (nn.dim==3){
			zz.new = array(NA, dim=c(nx.new, ny.new, nn.z))
		} else {
			zz.new = array(NA, dim=c(nx.new, ny.new))
		}
	}

	if (nn.dim==3){
		for (kk in 1:nn.z){
			for (ii in 1:nx.new){
				jj = trunc((ii-1)/div) + 1		
				if (flipped){
					zz.new[kk,,ii] = rep(grid[[para]][kk,,jj], each=div)
				} else {
					zz.new[ii,,kk] = rep(grid[[para]][jj,,kk], each=div)
				}
			}
		}
	} else {
		for (ii in 1:nx.new){
			jj = trunc((ii-1)/div) + 1		
			if (flipped){
				zz.new[,ii] = rep(grid[[para]][,jj], each=div)
			} else {
				zz.new[ii,] = rep(grid[[para]][jj,], each=div)
			}
		}
	}

	if (dens.cor){
		zz.new = zz.new/(div^2)
	}
	
	out.grid = list(xrng=xrng.new, yrng=yrng.new, dx=dx.new, dy=dy.new)
	out.grid[[para]] = zz.new
	#	copy additional fields
	for (ii in 1:length(grid)){
		if (names(grid)[ii] %in% c("xrng", "yrng", "dx", "dy", para)) next
		out.grid[[names(grid)[ii]]] = grid[[names(grid)[ii]]]
	}

	return(invisible(out.grid))
}


###	example 
#require(myRtools)
#
#org = list(xrng = c(0,10), yrng = c(0,10), zz = array(NA, c(11,11,10)))
#org$dx = diff(org$xrng)/(dim(org$zz)[1]-1)
#org$dy = diff(org$yrng)/(dim(org$zz)[2]-1)
#org$zz[,,1] = 1
#org$zz[5,2,1] = 2
#org$zz[3,1,1] = 3
#org$zz[10,9,2] = 4
##
#fill.grid(org, rotate=FALSE)
#
#div4 = divide.grid(org, div=2, flipped=FALSE, dens.cor=TRUE)
#
#fill.2dplot(div4, type="grid", grid.borders=1)
###
####tmp = pool.grid(div4, pp=4, FUN=sum)
