#'  Reading variable from netcdf file
#'
#'	Reads a single varialbe from a netcdf file assuming that the variable is given x,y coordinates 
#'	representing geographic location. 
#'	The function offers the possibility to restrict the range of the loaded data in x,y direction
#'	but also for any other additional dimension. If the dimension 
#'	'time' is contained in file, the function will try to convert its values into a 
#'	chron object making use of the attached units statement. This assumes that the time units 
#'	follow the format "<time units> since <ref date>". <time units> being on of 'seconds', 
#'	'minutes', 'hours', 'days' or 'years' and <ref date> being a reference date in ISO notation. 
#'  If netcdf file contains definition of a rotated pole (variable 'rotated_pole') its attributes
#'	are evaluated and proj4string for rotated longlat grid is created.
#'
#' @param fn Netcdf file name
#' @param sn short name of variable to load
#' @param xrng (two element numeric) restrict loading to specified x range (selection by value)
#' @param yrng (two element numeric) restrict loading to specified y range (selection by value)
#' @param dimrng (list) List of range restrictions for additional dimensions. Each list member
#'			containing the desired range by value (two element vector). If the dimension 
#'			'time' is contained in file, the function will try to convert its values into a 
#'			chron object making use of the attached units statement. In this case dimrng can 
#'			also be given as a chron object. 
#' @param proj4string Projection string associated with the data. Default is 
#'			"+proj=longlat +ellps=WGS84 +datum=WGS84" for a regular long/lat projection. 
#'
#' @return Loaded variable as reg.grid object, whith elements xrng, yrng, dx, dy, zz, 
#'			proj4string. Additional elements can be 'ext.dims' containing information on 
#'			additional dimensions and 'global.attr' containing any global attributes of the
#'			netcdf file. 
#' @export 
read.reg.grid.from.ncdf = function(fn, sn, xrng=NULL, yrng=NULL, dimrng=NULL, 
	proj4string="+proj=longlat +ellps=WGS84 +datum=WGS84"){

	require(ncdf4)

	xnames = c("lon", "lng", "longitude", "LON", "LONGITUDE", "Long", "Longitude", "x", "rlon")
	ynames = c("lat", "latitude", "LAT", "LATITUDE", "Lat", "Latitude", "y", "rlat")

	cn = nc_open(fn)

	#	varnames
	varnames = sapply(cn$var, "[[", "name")
	#	check if requested varname is in file
	if (! sn %in% varnames){
		cat("Selected variable name not in file.")
		print(cn)
		nc_close(cn)
		return(NULL)
	}

	#	try to find x and y dimnames
	dimnames = sapply(cn$dim, "[[", "name")	

	xidx = which(xnames %in% dimnames)
	if (length(xidx)>0){
		xname = xnames[xidx[1]]
	} else {
		xname = NULL	 
	}
	xidx = which(dimnames == xname)

	yidx = which(ynames %in% dimnames)
	if (length(yidx)>0){
		yname = ynames[yidx[1]]
	} else {
		yname = NULL	 
	}
	yidx = which(dimnames == yname)

	#	load dimensional variables
	if (!is.null(xname)){
		xx = as.vector(ncvar_get(cn, xname))
		if (!is.null(xrng)){
			x.msk = which(xx>=xrng[1] & xx<=xrng[2])
		} else {
			x.msk = 1:length(xx)
		}
		
		#	check consistency of x.msk
		if (length(x.msk) == 0){
			warning("Invalid range selected for dimension for x dimension")
			return(NULL)
		}
		if (any(is.na(x.msk))){
			warning("Invalid range selected for dimension for x dimension")
			return(NULL)
		}

		xrng = range(xx[x.msk])
		x.start = x.msk[1]
		x.count = length(x.msk)
	} else {
		xrng = NULL
		x.start = 1
		x.count = -1
		dx = NULL
	}

	if (!is.null(yname)){
		yy = as.vector(ncvar_get(cn, yname))	
		if (!is.null(yrng)){
			y.msk = which(yy>=yrng[1] & yy<=yrng[2])
		} else {
			y.msk = 1:length(yy)
		}
		#	check consistency of x.msk
		if (length(y.msk) == 0){
			warning("Invalid range selected for dimension for y dimension")
			return(NULL)
		}
		if (any(is.na(y.msk))){
			warning("Invalid range selected for dimension for y dimension")
			return(NULL)
		}
		yrng = range(yy[y.msk])
		y.start = y.msk[1]
		y.count = length(y.msk)
	} else {
		yrng = NULL
		y.start = 1
		y.count = -1	
		dy = NULL
	}

	start = c(x.start, y.start)
	count = c(x.count, y.count)

	dimnames.var = sapply(cn$var[[sn]]$dim, "[[", "name")
	dim.len = sapply(cn$var[[sn]]$dim, "[[", "len")
	
	ext.dim.names = dimnames.var[!dimnames.var %in% c(xname, yname)]
	if (length(ext.dim.names)>0){
		ext.dims = vector("list", length(ext.dim.names))
		names(ext.dims) = ext.dim.names

		for (ii in 1:length(ext.dims)){
			ext.dims[[ii]]$name = names(ext.dims)[ii] 
			ext.dims[[ii]]$vals = as.vector(ncvar_get(cn, ext.dims[[ii]]$name))

			tmp = ncatt_get(cn, ext.dims[[ii]]$name, "units")
			if(tmp$hasatt) ext.dims[[ii]]$units = tmp$value

			tmp = ncatt_get(cn, ext.dims[[ii]]$name, "long_name")
			if(tmp$hasatt) ext.dims[[ii]]$longname = tmp$value

			if (ext.dims[[ii]]$name=="time"){
				tmp = ncatt_get(cn, ext.dims[[ii]]$name, "calendar")
				if (tmp$hasatt) ext.dims[[ii]]$calendar = tmp$value

				#	try to convert to chron, assuming that units follows the structure:
				#		<time units> since <ref date>
				tmp = strsplit(ext.dims[[ii]]$units, split="[ ]+")[[1]]
				if (tmp[2]=="since") {
					dtm.ref = string.2.chron(paste(tmp[3:length(tmp)], collapse=" "))
					dt = switch(tmp[1], seconds=1/86400, minutes=1/1440, hours=1/24, days=1, 
						years=365)
					ext.dims[[ii]]$vals.chron = dtm.ref + dt * ext.dims[[ii]]$vals
				} else {
					warning("Cannot convert time units to chron")
				}
			}

			if (ext.dims[[ii]]$name %in% names(dimrng)){
				#	if dimesion is time (chron)
				if (class(dimrng[[ext.dims[[ii]]$name]])=="chron" && 
					!is.null(ext.dims[[ii]]$vals.chron)) {
					dim.msk = which(
						ext.dims[[ii]]$vals.chron>=dimrng[[ext.dims[[ii]]$name]][1] & 
						ext.dims[[ii]]$vals.chron<=dimrng[[ext.dims[[ii]]$name]][2] )
				} else {	#	default numeric
					dim.msk = which(
						ext.dims[[ii]]$vals>=dimrng[[ext.dims[[ii]]$name]][1] & 
						ext.dims[[ii]]$vals<=dimrng[[ext.dims[[ii]]$name]][2] )
				}

				#	check consistency of dim.msk
				if (length(dim.msk) == 0){
					warning(paste("Invalid range selected for dimension",ext.dims[[ii]]$name))
					return(NULL)
				}
				if (any(is.na(dim.msk))){
					warning(paste("Invalid range selected for dimension",ext.dims[[ii]]$name))
					return(NULL)
				}
	
				#	assign start/end
				ext.dims[[ii]]$start = dim.msk[1]
				ext.dims[[ii]]$count = length(dim.msk)			
				ext.dims[[ii]]$vals = ext.dims[[ii]]$vals[dim.msk]
				if (!is.null(ext.dims[[ii]]$vals.chron)){
					ext.dims[[ii]]$vals.chron = ext.dims[[ii]]$vals.chron[dim.msk]
				}
			} else {
				ext.dims[[ii]]$start = 1
				ext.dims[[ii]]$count = -1
			}
		}
		start = c(start, sapply(ext.dims, "[[", "start"))
		count = c(count, sapply(ext.dims, "[[", "count"))

	} else {
		ext.dims = NULL
	}
	
	#	get the data 
	zz = ncvar_get(cn, sn, start=start, count=count)	

	#	set grid resolution 
	if (!is.null(xrng)){
		dx = diff(xrng)/(dim(zz)[1]-1)
	}
	if (!is.null(yrng)){
		dy = diff(yrng)/(dim(zz)[2]-1)
	}

	#	check for rotated pole
	if ("rotated_pole" %in% names(cn$var)) {
		p0lat = ncatt_get(cn, "rotated_pole", "grid_north_pole_latitude")$value
        p0lon = ncatt_get(cn, "rotated_pole", "grid_north_pole_longitude")$value

		proj4string = paste0("+proj=ob_tran +o_proj=longlat +o_lon_p=", p0lon, 
			" +o_lat_p=", p0lat, " +lon_0=180")
	}

	#	construct grid object
	grid = list(xrng=xrng, yrng=yrng, dx=dx, dy=dy, zz=zz, proj4string=proj4string)
	class(grid) = "reg.grid"

	if (!is.null(ext.dims)){
		grid$ext.dims = ext.dims
	}

	#	get all global attributes
	grid$global.attrs = ncatt_get(cn,0)
	if (length(grid$global.attrs)>0){
		grid$global.attrs = mapply(function(x,y){
				z = list(val=x)
				if (class(x)=="character"){
					z$prec = "text"
				} else if (class(x)=="numeric"){
					z$prec = "double"
				} else if (class(x)=="integer"){
					z$prec = "short"
				} else {
					z$prec = NA
				}
				z$name = y
				return(z)
			}, grid$global.attrs, names(grid$global.attrs), SIMPLIFY=FALSE)
	} else {
		grid$global.attrs = NULL
	}

	#	close file again
	nc_close(cn)
	

	return(grid)
}
