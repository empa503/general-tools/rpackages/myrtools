###############################################################################
#             cut.dtm
###############################################################################
# purpose:    "cut.dtm" divides the range of "x" into intervals and codes the values
#               in x according to which interval they fall.
# arguments:    x           (POSIXt)    vector of time stamps
#               breaks      length of intervals given as a character string ("secs", "mins", "hours", 
#                           "days", "weeks", "months", "years", "DSTdays")
#                           if given as a numeric 'breaks' specifies the interval length in seconds
#               labels      ???
#               start.on.monday (logical) if TRUE first day of the week is monday, sunday otherwise
#               right       (logical) indicating if the intervals should be closed on the right (and open on the left) or vice versa.
#               ...         other arguments passed to 'cut'
# returns:    A 'factor' is returned
# author:     joerg.klausen@empa.ch
#             stephan.henne@empa.ch
# version:    1.0-060509
#             1.0-060628    enabled numeric breaks specification (sec)
# changes: 
###############################################################################
cut.dtm = function (x, breaks, labels = NULL, start.on.monday = TRUE, right = TRUE, ...) {
    if (!inherits(x, "POSIXt")) 
        stop("`x' must be a date-time object")
    x <- as.POSIXct(x, tz = "GMT")
    if (inherits(breaks, "POSIXt")) {
        breaks <- as.POSIXct(breaks)
    }
    else if (is.numeric(breaks) && length(breaks) == 1) {
    #   if breaks are specified as numeric
        #   this looks a little bit messy, however using POSIXct messes timezones up again ...
        #   whenever you touch a POSIXct object it seems to forget about the timezone
        #   using chron seems to work
        tmp.x = as.numeric(as.chron(x))*86400
        maxx <- as.chron(max(tmp.x, na.rm = TRUE)/86400)
        start <- trunc(as.numeric(min(tmp.x, na.rm = TRUE))/breaks)*breaks        
        start = as.chron(start/86400)               
        class(start) = c("chron", "dates", "times")
        attr(start, "format") = c("y-m-d", "h:m:s")
        breaks = breaks / 86400
		dts = seq.dates(start, maxx+breaks, breaks)
		#	fix loss of chron class for only midnight time stamps
		if (class(dts)[1]!="chron"){
			attr(dts, "format") = c("y-m-d", "h:m:s")
			class(dts) = c("chron", "dates", "times")
		}
        breaks = round(dts, "secs")
        breaks = round(as.POSIXlt(breaks, tz="GMT"), "secs")
        breaks = as.POSIXct(breaks)
    }
    else if (is.character(breaks) && length(breaks) == 1) {        
        by2 <- strsplit(breaks, " ", fixed = TRUE)[[1]]
        if (length(by2) > 2 || length(by2) < 1) 
            stop("invalid specification of `breaks'")
        valid <- pmatch(by2[length(by2)], c("secs", "mins", "hours", 
            "days", "weeks", "months", "years", "DSTdays"))
        if (is.na(valid)) 
            stop("invalid specification of `breaks'")
        start <- as.POSIXlt(min(x, na.rm = TRUE), tz = "GMT")
        incr <- 1
        if (valid > 1) {
            start$sec <- 0
            incr <- 60 
        }
        if (valid > 2) {
            start$min <- 0
            incr <- 3600
        }
        if (valid > 3) {
            start$hour <- 0
            incr <- 86400
        }
        if (valid == 5) {
            start$mday <- start$mday - start$wday
            if (start.on.monday) 
                start$mday <- start$mday + ifelse(start$wday > 0, 1, -6)
            incr <- 7 * 86400
        }
        if (valid == 6) {
            start$mday <- 1
            incr <- 31 * 86400
        }
        if (valid == 7) {
            start$mon <- 0                       
            start$mday <- 1
            incr <- 366 * 86400
        }
        if (valid == 8) 
            incr <- 25 * 3600
        if (length(by2) == 2) 
            incr <- incr * as.integer(by2[1])
        maxx <- max(x, na.rm = TRUE)               
        breaks <- seq(start, maxx + incr, breaks)        
        breaks = as.POSIXct(breaks, tz="GMT")
#        breaks <- breaks[1:(1 + max(which(breaks < maxx)))]
    } else {
		stop("invalid specification of `breaks'")
	}

    res <- cut(unclass(x), unclass(breaks), labels = labels, right = right, ...)

    if (is.null(labels)) {
        levels(res) <- as.character(breaks[-length(breaks)])
	}

	return(res)
}
