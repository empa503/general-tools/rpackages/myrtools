bcuint = function(y, y1, y2, y12, x1l, x1u, x2l, x2u, x1, x2){
   verbose = FALSE
    
    ansy = 0.0
    ansy1 = 0.0
    ansy2=0.0
   #    check length and type of variables before passing them
    if (length(y)>4) y = y[1:4] 
    if (length(y)<4) stop("y has to have 4 elements")
    if (any(is.na(y))) stop("y contains NA, not allowed in foreign function call")
    
    if (length(y1)>4) y1 = y1[1:4] 
    if (length(y1)<4) stop("y1 has to have 4 elements")
    if (any(is.na(y1))) stop("y1 contains NA, not allowed in foreign function call")

    if (length(y2)>4) y2 = y2[1:4] 
    if (length(y2)<4) stop("y2 has to have 4 elements")
    if (any(is.na(y2))) stop("y2 contains NA, not allowed in foreign function call")

    if (length(y12)>4) y12 = y12[1:4] 
    if (length(y12)<4) stop("y12 has to have 4 elements")
    if (any(is.na(y12))) stop("y12 contains NA, not allowed in foreign function call")
  
    if (length(x1l)>1) x1l = x1l[1] 
    if (length(x1l)<1) stop("x1l has to have 1 element")
    if (any(is.na(x1l))) stop("x1l contains NA, not allowed in foreign function call")
  
    if (length(x1u)>1) x1u = x1u[1] 
    if (length(x1u)<1) stop("x1u has to have 1 element")
    if (any(is.na(x1u))) stop("x1u contains NA, not allowed in foreign function call")

    if (length(x2l)>1) x2l = x2l[1] 
    if (length(x2l)<1) stop("x2l has to have 1 element")
    if (any(is.na(x2l))) stop("x2l contains NA, not allowed in foreign function call")
  
    if (length(x2u)>1) x2u = x2u[1] 
    if (length(x2u)<1) stop("x2u has to have 1 element")
    if (any(is.na(x2u))) stop("x2u contains NA, not allowed in foreign function call")

    if (length(x1)>1) x1 = x1[1] 
    if (length(x1)<1) stop("x1 has to have 1 element")
    if (any(is.na(x1))) stop("x1 contains NA, not allowed in foreign function call")

    if (length(x2)>1) x2 = x2[1] 
    if (length(x2)<1) stop("x2 has to have 1 element")
    if (any(is.na(x2))) stop("x2 contains NA, not allowed in foreign function call")
   #    test relations x1, x2
    if (x1<x1l || x1>x1u) stop("x1 out of range")
    if (x2<x2l || x2>x2u) stop("x2 out of range")
    #   verbose print
    if (verbose){
        print(y)
        print(y1)
        print(y2)
        print(y12)
        print(c(x1l, x1u))
        print(c(x2l, x2u))
        print(c(x1, x2))
    }
   #    
    res = .C("R_bcuint", as.single(c(0, y)), as.single(c(0, y1)), as.single(c(0, y2)), as.single(c(0, y12)), 
        as.single(x1l), as.single(x1u), as.single(x2l), as.single(x2u),
        as.single(x1), as.single(x2), ansy=as.single(ansy), ansy1=as.single(ansy1), ansy2=as.single(ansy2))
    
    res = res$ansy
    attr(res, "Csingle") = NULL
    
    return(res)
}
