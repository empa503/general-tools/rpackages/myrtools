
stereo.sphr.ll = function (x, y, PROJ.DATA)
{
    k0 = 1
    lam0 = PROJ.DATA$LON0
    phi1 = PROJ.DATA$LAT0
    phi1 = phi1 * pi/180
    lam0 = lam0 * pi/180
    R = MAPconstants()$A.MAPK
    rho = sqrt(x^2 + y^2)
    cee = 2 * atan2(rho, 2 * R * k0)
    phi = asin(cos(cee) * sin(phi1) + y * sin(cee) * cos(phi1)/rho)
    if (PROJ.DATA$LAT1 == 90) {
        lam = lam0 + atan2(x, (-y))
    }
    if (PROJ.DATA$LAT1 == (-90)) {
        lam = lam0 + atan2(x, (y))
    }
    if (PROJ.DATA$LAT1 != (-90) & PROJ.DATA$LAT1 != (90)) {
        lam = lam0 + atan2(x * sin(cee), rho * cos(phi1) * cos(cee) - 
            y * sin(phi1) * sin(cee))
    }
    lon = (lam) * 180/pi
    lat = (phi) * 180/pi
    
    lon[lon>180] = lon[lon>180] - 360
    lon[lon<(-180)] = lon[lon<(-180)] + 360    
    
    return(list(lat = lat, lon = lon))
}
