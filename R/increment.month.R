increment.month = function (x, by = 1) {
    require(chron)
    if (!class(x)[1] %in% c("chron", "dates", "times")) 
        stop("x must be a dates or chron object")
    if (by > 12 || by < 0) 
        stop("by must be within [0, 12]")
    tme = chron::times(x)%%1
    dte = dates(x)
    dte = with(month.day.year(dte), {
        year = ifelse(month > (12 - by), year + 1, year)
        month = ifelse(month > (12 - by), month - (12 - by), 
            month + by)
        chron(paste(month, day, year, sep = "/"))
    })
#    return(dte + tme)
    return(chron(dte, tme, out.format = c("y-m-d", "h:m:s")))
}
