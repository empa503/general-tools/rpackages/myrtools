ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine R_DGESVX(a,b,x,nn,ll)
! i/o
      real*8, dimension(nn,nn) :: a 
      real*8, dimension(nn,ll) :: b 
      real*8, dimension(nn,ll) :: x 
      integer nn,ll

      real*8, dimension(nn,nn) :: af
      integer, dimension(nn) :: ipiv, iwork
      double precision, dimension(nn) :: r,c, ferr, berr
      double precision, dimension(nn*4) :: work
      double precision :: rcond 
      character equed
      integer :: ii, info

      call DGESVX('E', 'N', nn, ll, a, nn, af,nn,ipiv, equed, r, c, b, 
     + nn, x, nn, rcond, ferr, berr, work, iwork, info) 

      end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

