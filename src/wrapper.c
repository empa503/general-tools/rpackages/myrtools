#include <stdlib.h>
#include <math.h>

extern void bcuint(float y[], float y1[], float y2[], float y12[], float x1l,
    float x1u, float x2l, float x2u, float x1, float x2, float *ansy,
    float *ansy1, float *ansy2);

extern void bliint(double y[], double x1l,
    double x1u, double x2l, double x2u, double x1, double x2, double *ansy);


void R_bliint(double y[], double *x1l,
    double *x1u, double *x2l, double *x2u, double *x1, double *x2, double *ansy)
{
//    Rprintf("%f %f %f %f\n", *x1l, *x1u, *x2l, *x2u);
//    Rprintf("%f %f %f %f\n", y[1], y[2], y[3], y[4]);
//	Rprintf("%f %f\n", *x1, *x2);
    bliint(y, *x1l, *x1u, *x2l, *x2u, *x1, *x2, ansy);
//    Rprintf("%f\n", *ansy);
}

void R_resample_grid(int ix[], int jy[], double zz[], int *nn, int *method, 
	int *nx, int *ny, int *nz, double new[], int count[]){ 

	register int ii;
	register int idx;
	
	//	count.fac, most
	if ((*method)==3 | (*method)==6){
		for (ii=0; ii<*nn; ii++) {
			idx = ((int)zz[ii]-1)*(*nx)*(*ny) + (jy[ii]-1)*(*nx) + ix[ii]-1 ;
//			Rprintf("%d %d %d\n", ii, (int)zz[ii], idx);
			new[idx]++;
		}
	//	count
	} else if (*method==7){
		for (ii=0; ii<*nn; ii++) {
			idx = (jy[ii]-1)*(*nx) + ix[ii]-1 ;
			new[idx]++;
		}
	//	mean, sum, frac.above.mean
	} else if (*method==2 || *method==1 || *method==8){
		for (ii=0; ii<*nn; ii++) {
			idx = (jy[ii]-1)*(*nx) + ix[ii]-1 ;
			new[idx]+= zz[ii];
			count[idx]++;
		}
	//	min
	} else if (*method==5){
		for (ii=0; ii<*nn; ii++) {
			idx = (jy[ii]-1)*(*nx) + ix[ii]-1 ;
			if (zz[ii] < new[idx]) new[idx] = zz[ii] ;  			
		}
	//	max
	} else if (*method==4){
		for (ii=0; ii<*nn; ii++) {
			idx = (jy[ii]-1)*(*nx) + ix[ii]-1 ;
			if (zz[ii] > new[idx]) new[idx] = zz[ii];
		}
	}

}

void R_bliint_reduced_gg(float x[], float y[], float z[], int* np, float *vals, 
	float *zz, float *lats, float *lons, int *nx, int *ny, int *nz, float *ansy){

	int ij_ll, ij_lr, ij_ul, ij_ur;
	int kz_ll, kz_lr, kz_ul, kz_ur;
	int kz_llm, kz_lrm, kz_ulm, kz_urm;
	float val_ll, val_lr, val_ul, val_ur;
	int ii, jj, kk;
	int ix_top, ix_bot, jy, jyp;
	int ng;
	int *frst;
	float dz1, dz2, dd[4];
	float pi;
	int ur_circ, lr_circ;

	pi = 4*atan(1);

	frst = (int*) calloc(*ny, sizeof(int));
	frst[0] = 0;
	ng = 0;
	// number of grid points
	for (ii=0; ii<*ny; ii++){
		ng += nx[ii];
	}
	// frst index of new lat band
	for (ii=1; ii<*ny; ii++) {
		frst[ii] = frst[ii-1] + nx[ii-1];
	}

	// loop over all points
	for (ii=0; ii<*np; ii++){
		for (jy=(*ny-1); jy>-1; jy--){
			if (lats[frst[jy]]>=y[ii]) break;
		}
		if (jy==(*ny-1)){
			jyp = jy;
		} else if (jy==0){
			jyp = jy;
		} else {
			jyp = jy +1;
		}
//		Rprintf("%d\n", jyp);
		for (ix_top=0; ix_top<nx[jy]; ix_top++){
			if (x[ii]<=lons[frst[jy]+ix_top]) break;
		}

		for (ix_bot=0; ix_bot<nx[jy]; ix_bot++){
			if (x[ii]<=lons[frst[jyp]+ix_bot]) break;
		}

		ij_ul = frst[jy] + ix_top;
		if (ix_top==(nx[jy]-1)){
			ij_ur = frst[jy];
			ur_circ = 1;
		} else {
			ij_ur = frst[jy] + ix_top +1;
			ur_circ = 0;
		}

		ij_ll = frst[jyp] + ix_bot;
		if (ix_bot==(nx[jyp]-1)){
			ij_lr = frst[jyp];
			lr_circ = 1;
		} else {
			ij_lr = frst[jyp] + ix_bot +1;
			lr_circ = 0;
		}

//		Rprintf("%d %d %d %d\n", jy, jyp, ix_top, ix_bot);
//		Rprintf("UL: %d %f %f\n", ij_ul, lons[ij_ul], lats[ij_ul]);
//		Rprintf("UR: %f %f\n", lons[ij_ur], lats[ij_ur]);
//		Rprintf("LL: %f %f\n", lons[ij_ll], lats[ij_ll]);
//		Rprintf("LR: %f %f\n", lons[ij_lr], lats[ij_lr]);
		

//		VERTICAL INTERPOLATION
		for (kz_ll=0; kz_ll<*nz; kz_ll++) if (zz[kz_ll*ng+ij_ll]>z[ii]) break;
		kz_llm = kz_ll - 1;

		for (kz_lr=0; kz_lr<*nz; kz_lr++) if (zz[kz_lr*ng+ij_lr]>z[ii]) break;
		kz_lrm = kz_lr -1;

		for (kz_ul=0; kz_ul<*nz; kz_ul++) if (zz[kz_ul*ng+ij_ul]>z[ii]) break;
		kz_ulm = kz_ul - 1;

		for (kz_ur=0; kz_ur<*nz; kz_ur++) if (zz[kz_ur*ng+ij_ur]>z[ii]) break;
		kz_urm = kz_ur - 1;

		if(kz_llm<0){
			val_ll = vals[(kz_ll)*ng+ij_ll];
		} else if (kz_ll==(*nz)){
			val_ll = vals[(kz_llm)*ng+ij_ll];	
		} else {
			dz2 = (z[ii]-zz[(kz_llm)*ng + ij_ll])/(zz[(kz_ll)*ng + ij_ll]- zz[(kz_llm)*ng + ij_ll]);
			dz1 = 1 - dz2;
			val_ll = dz1 * vals[(kz_llm)*ng+ij_ll]  + dz2 * vals[(kz_ll)*ng+ij_ll];
		}

		if (kz_lrm<0){
			val_lr = vals[(kz_lr)*ng+ij_lr];
		} else if (kz_lr==(*nz)){
			val_lr = vals[(kz_lrm)*ng+ij_lr];	
		} else {
			dz2 = (z[ii]-zz[(kz_lrm)*ng + ij_lr])/(zz[(kz_lr)*ng + ij_lr]- zz[(kz_lrm)*ng + ij_lr]);
			dz1 = 1 - dz2;
			val_lr = dz1 * vals[(kz_lrm)*ng+ij_lr]  + dz2 * vals[(kz_lr)*ng+ij_lr];
		}
	
		if (kz_urm<0){
			val_ur = vals[(kz_ur)*ng+ij_ur];
		} else if (kz_ur==(*nz)){
			val_ur = vals[(kz_urm)*ng+ij_ur];	
		} else {
			dz2 = (z[ii]-zz[(kz_urm)*ng + ij_ur])/(zz[(kz_ur)*ng + ij_ur]- zz[(kz_urm)*ng + ij_ur]);
			dz1 = 1 - dz2;
			val_ur = dz1 * vals[(kz_urm)*ng+ij_ur]  + dz2 * vals[(kz_ur)*ng+ij_ur];
		}

		if (kz_ulm<0){
			val_ul = vals[(kz_ul)*ng+ij_ul];
		} else if (kz_ul==(*nz)){
			val_ul = vals[(kz_ulm)*ng+ij_ul];	
		} else {
			dz2 = (z[ii]-zz[(kz_ulm)*ng + ij_ul])/(zz[(kz_ul)*ng + ij_ul]- zz[(kz_ulm)*ng + ij_ul]);
			dz1 = 1 - dz2;
			val_ul = dz1 * vals[(kz_ulm)*ng+ij_ul]  + dz2 * vals[(kz_ul)*ng+ij_ul];
		}

//		HORIZONTAL INTERPOLATION
		dd[0] = sqrt(pow((x[ii]-lons[ij_ll])*cos(lats[ij_ll]/180.*pi), 2) + 
			pow(y[ii]-lats[ij_ll],2));
		if (dd[0]==0.){
			ansy[ii] = val_ll;
			continue;
		}

		if (lr_circ==1){
			dd[1] = sqrt(pow((x[ii]-lons[ij_lr]-360.)*cos(lats[ij_lr]/180.*pi), 2) + 
				pow(y[ii]-lats[ij_lr],2));
		} else {
			dd[1] = sqrt(pow((x[ii]-lons[ij_lr])*cos(lats[ij_lr]/180.*pi), 2) + 
				pow(y[ii]-lats[ij_lr],2));
		}
		if (dd[1]==0.){
			ansy[ii] = val_lr;
			continue;
		}
		if (ur_circ==1){
			dd[2] = sqrt(pow((x[ii]-lons[ij_ur]-360.) * cos(lats[ij_ur]/180.*pi), 2) + 
				pow(y[ii]-lats[ij_ur],2));
		} else {
			dd[2] = sqrt(pow((x[ii]-lons[ij_ur]) * cos(lats[ij_ur]/180.*pi), 2) + 
				pow(y[ii]-lats[ij_ur],2));
		}
		if (dd[2]==0.){
			ansy[ii] = val_ur;
			continue;
		}

		dd[3] = sqrt(pow((x[ii]-lons[ij_ul]) * cos(lats[ij_ul]/180.*pi), 2) + 
			pow(y[ii]-lats[ij_ul],2));
		if (dd[3]==0.){
			ansy[ii] = val_ul;
			continue;
		}
		//Rprintf("%f %f %f %f\n", dd[0], dd[1], dd[2], dd[3]);
		//	invert distances
		for (jj=0; jj<4; jj++) dd[jj] = 1./dd[jj];

		//	distance weighted average
		ansy[ii] = (dd[0]*val_ll+dd[1]*val_lr+dd[2]*val_ur+dd[3]*val_ul)/(dd[0]+dd[1]+dd[2]+dd[3]);

//        bliint(yf, lons[ij_ll], lons[ij_lr], lats[ij_ll], lats[ij_ul], x[ii], y[ii], &ansy[ii]);
	}

	free(frst);
}

void R_bliint_grid_3d(double y[], int* nx, int* ny, int* nz, double z[], 
	double *x1l, double *x1u, double *x2l, double *x2u, 
	double *x1, double *x2, double* x3, int* nn, 
	double *missval, double *ansy){

    double ***yy ;
    double ***zz ;
    double dx, dy;
	double dzz, dz1, dz2;
    register int ii, jj, kk, kz, kzp;
    int idxi, idxj;
    double yf[5];
	double wf;
	double wsum, ysum;
	double x1f;
	double x2f;
    double x1uf, x1lf, x2uf, x2lf;
	int cnt;

    dx = (*x1u-*x1l)/(*nx-1);
    dy = (*x2u-*x2l)/(*ny-1);

    // allocate arrays
    yy = (double***) calloc(*nx, sizeof(double**));
    zz = (double***) calloc(*nx, sizeof(double**));
    for (ii=0; ii<*nx; ii++){
        yy[ii] = (double**) calloc(*ny, sizeof(double*));
        zz[ii] = (double**) calloc(*ny, sizeof(double*));
		for (jj=0; jj<*ny; jj++){
        	yy[ii][jj] = (double*) calloc(*nz, sizeof(double));
        	zz[ii][jj] = (double*) calloc(*nz, sizeof(double));
		}
    }

    //  reconvert y to matrix
	for (kk=0; kk<*nz; kk++){
	    for (jj=0; jj<*ny; jj++){
    	    for (ii=0; ii<*nx; ii++){
        	    yy[ii][jj][kk] = y[ii + (jj + kk*(*ny))*(*nx)];
        	    zz[ii][jj][kk] = z[ii + (jj + kk*(*ny))*(*nx)];
	        }
    	}
	}
//	for (kk=0; kk<*nz; kk++){
//		Rprintf("%f %f\n", zz[5][7][kk], yy[5][7][kk]);
//	}

    // estimate neighbouring grid cells
    for(ii=0; ii<*nn; ii++){
        idxi = (x1[ii] - *x1l)/dx;
        idxj = (x2[ii] - *x2l)/dy;
        // make sure to stay within grid
        if (idxi==(*nx-1))  idxi = idxi-1;
        if (idxj==(*ny-1))  idxj = idxj-1;
        // otherwise set to NA
        if (idxi>(*nx-1) || idxj>(*ny-1) || idxi<0 || idxj<0) {
            ansy[ii] = *missval ;
            continue ;
        }
//		Rprintf("ix: %d, jy: %d\n", idxi, idxj);

    // construct input for bcuint
		for (kzp=0; kzp<=(*nz); kzp++) if (zz[idxi][idxj][kzp]>=x3[ii]) break;
		//Rprintf("kz: %d, kzp: %d\n", kz, kzp);

		if (kzp>=(*nz)) kzp = *nz - 1;
		if (kzp>0) kz = kzp-1; else kz=0;
		dzz = zz[idxi][idxj][kzp] - zz[idxi][idxj][kz];
		
		//Rprintf("ix: %d, jy: %d\n", idxi, idxj);
		//Rprintf("kz: %d, kzp: %d\n", kz, kzp);

		if (dzz > 0.) {
			dz2 = (x3[ii] - zz[idxi][idxj][kz])/dzz;
			dz1 = (zz[idxi][idxj][kzp] - x3[ii])/dzz;
		} else {
			dz1 = 1;
			dz2 = 0;
		}
		//Rprintf("x3: %f, zz[kz]: %f, zz[kzp]: %f\n", x3[ii], zz[idxi][idxj][kz], 
		//	x3[ii], zz[idxi][idxj][kz]);
		//Rprintf("dzz: %f, dz1: %f, dz2: %f\n", dzz, dz1, dz2);
        yf[1] = yy[idxi][idxj][kz]*dz1 + yy[idxi][idxj][kzp]*dz2;
//		Rprintf("%d %f %f %f %f %f\n", kz, zz[idxi][idxj][kz], zz[idxi][idxj][kzp], dz1, dz2, yf[1]);

		for (kzp=0; kzp<=(*nz); kzp++) if (zz[idxi+1][idxj][kzp]>=x3[ii]) break;
		if (kzp>=(*nz)) kzp = *nz - 1;
		if (kzp>0) kz = kzp-1; else kz=0;
		//Rprintf("kz: %d, kzp: %d\n", kz, kzp);
		dzz = zz[idxi+1][idxj][kzp] - zz[idxi+1][idxj][kz];
		if (dzz > 0.) {
			dz2 = (x3[ii] - zz[idxi+1][idxj][kz])/dzz;
			dz1 = (zz[idxi+1][idxj][kzp] - x3[ii])/dzz;
		} else {
			dz1 = 0.5;
			dz2 = 0.5;
		}
        yf[2] = yy[idxi+1][idxj][kz]*dz1 + yy[idxi+1][idxj][kzp]*dz2;

		for (kzp=0; kzp<=(*nz); kzp++) if (zz[idxi+1][idxj+1][kzp]>=x3[ii]) break;
		if (kzp>=(*nz)) kzp = *nz - 1;
		if (kzp>0) kz = kzp-1; else kz=0;
		//Rprintf("kz: %d, kzp: %d\n", kz, kzp);
		dzz = zz[idxi+1][idxj+1][kzp] - zz[idxi+1][idxj+1][kz];
		if (dzz > 0.) {
			dz2 = (x3[ii] - zz[idxi+1][idxj+1][kz])/dzz;
			dz1 = (zz[idxi+1][idxj+1][kzp] - x3[ii])/dzz;
		} else {
			dz1 = 1;
			dz2 = 0;
		}
        yf[3] = yy[idxi+1][idxj+1][kz]*dz1 + yy[idxi+1][idxj+1][kzp]*dz2;

		for (kzp=0; kzp<=(*nz); kzp++) if (zz[idxi][idxj+1][kzp]>=x3[ii]) break;
		if (kzp>=(*nz)) kzp = *nz - 1;
		if (kzp>0) kz = kzp-1; else kz=0;

		//Rprintf("kz: %d, kzp: %d\n", kz, kzp);
		dzz = zz[idxi][idxj+1][kzp] - zz[idxi][idxj+1][kz];
		if (dzz > 0.) {
			dz2 = (x3[ii] - zz[idxi][idxj+1][kz])/dzz;
			dz1 = (zz[idxi][idxj+1][kzp] - x3[ii])/dzz;
		} else {
			dz1 = 1;
			dz2 = 0;
		}
        yf[4] = yy[idxi][idxj+1][kz]*dz1 + yy[idxi][idxj+1][kzp]*dz2;


		if (yf[1]==*missval || yf[2]==*missval || yf[3]==*missval || yf[4]==*missval) {
			cnt=0;
			wsum=0.;
			ysum=0.;
			for (jj=1; jj<=4; jj++){
				if (yf[jj]!=*missval){
					if ((cnt+1)<jj){
						yf[cnt+1] = yf[jj]; 
					}
					x1f = *x1l + dx*(idxi+(jj/2 % 2));
					x2f = *x2l + dy*(idxj+jj/3); 
					//inverse distance weight
					wf = sqrt(pow(x1[ii]-x1f,2) + pow(x2[ii]-x2f,2));
					if (wf > 0.) {
						wf = 1./wf;
						wsum+= wf;
						ysum+= wf*yf[cnt+1];
					} else {
						// at exact position as grid point 
						ansy[ii] = yf[cnt+1];
						cnt = -1;
						break;
					}
					cnt++;
				}
			}
			if (cnt==0){
				ansy[ii] = *missval;
			} else if (cnt>0){
				ansy[ii] = ysum/wsum ;
			} 
			
		} else {

    	// bilinear interpolation
	        x1uf = *x1l + dx*(idxi+1);
    	    x1lf = *x1l + dx*(idxi);
        	x2uf = *x2l + dy*(idxj+1);
	        x2lf = *x2l + dy*(idxj);

            //Rprintf("x: %f, y: %f\n", x1[ii], x2[ii]);
			//Rprintf("xl: %f, xr: %f, yb: %f, yt: %f\n", x1lf, x1uf, x2lf, x2uf);

	        bliint(yf, x1lf, x1uf, x2lf, x2uf, x1[ii], x2[ii], &ansy[ii]);
			//Rprintf("%f %f %f %f; res: %f\n", yf[1], yf[2], yf[3], yf[4], ansy[ii]);
		}

    }

    // free arrays
    for (ii=0; ii<*nx; ii++){
		for (jj=0; jj<*ny; jj++){
        	free(yy[ii][jj]);
	        free(zz[ii][jj]);
		}
        free(yy[ii]);
        free(zz[ii]);
    }
    free(yy);
    free(zz);

}
void R_bliint_grid(double y[], int* nx, int* ny, double *x1l, double *x1u,
	double *x2l, double *x2u, double *x1, double *x2, int* nn, double *missval, double *ansy){

    double **yy ;
    double dx, dy;
    register int ii, jj;
    int idxi, idxj;
    double yf[5];
	double wf;
	double wsum, ysum;
	double x1f;
	double x2f;
    double x1uf, x1lf, x2uf, x2lf;
	int cnt;

    dx = (*x1u-*x1l)/(*nx-1);
    dy = (*x2u-*x2l)/(*ny-1);

    // allocate arrays
    yy = (double**) calloc(*nx, sizeof(double*));

    for (ii=0; ii<*nx; ii++){
        yy[ii] = (double*) calloc(*ny, sizeof(double));
    }

    //  reconvert y to matrix
    for (jj=0; jj<*ny; jj++){
        for (ii=0; ii<*nx; ii++){
            yy[ii][jj] = y[(ii*(*ny))+jj];
        }
    }

    // estimate neighbouring grid cells
    for(ii=0; ii<*nn; ii++){
        idxi = (x1[ii] - *x1l)/dx;
        idxj = (x2[ii] - *x2l)/dy;
        // make sure to stay within grid
        if (idxi==(*nx-1))  idxi = idxi-1;
        if (idxj==(*ny-1))  idxj = idxj-1;
        // otherwise set to NA
        if (idxi>(*nx-1) || idxj>(*ny-1) || idxi<0 || idxj<0) {
            ansy[ii] = *missval ;
//            Rprintf("%d\n", ii);
            continue ;
        }

//        Rprintf("%f %d %f %d\n", x1[ii], idxi , x2[ii], idxj);

    // construct input for bcuint
        yf[1] = yy[idxi][idxj];
        yf[2] = yy[idxi+1][idxj];
        yf[3] = yy[idxi+1][idxj+1];
        yf[4] = yy[idxi][idxj+1];

		if (yf[1]==*missval || yf[2]==*missval || yf[3]==*missval || yf[4]==*missval) {
			cnt=0;
			wsum=0.;
			ysum=0.;
			for (jj=1; jj<=4; jj++){
				if (yf[jj]!=*missval){
					if ((cnt+1)<jj){
						yf[cnt+1] = yf[jj]; 
					}
					x1f = *x1l + dx*(idxi+(jj/2 % 2));
					x2f = *x2l + dy*(idxj+jj/3); 
					//inverse distance weight
					wf = sqrt(pow(x1[ii]-x1f,2) + pow(x2[ii]-x2f,2));
					if (wf > 0.) {
						wf = 1./wf;
						wsum+= wf;
						ysum+= wf*yf[cnt+1];
					} else {
						// at exact position as grid point 
						ansy[ii] = yf[cnt+1];
						cnt = -1;
						break;
					}
//				        Rprintf("%d %f %f %f\n", jj, yf[cnt+1], wf, ysum);
					cnt++;
				}
			}
			if (cnt==0){
				ansy[ii] = *missval;
			} else if (cnt>0){
				ansy[ii] = ysum/wsum ;
			} 
			

		} else {

    	// bilinear interpolation
	        x1uf = *x1l + dx*(idxi+1);
    	    x1lf = *x1l + dx*(idxi);
        	x2uf = *x2l + dy*(idxj+1);
	        x2lf = *x2l + dy*(idxj);

//        Rprintf("%f %f\n", x1[ii], x2[ii]);
	        bliint(yf, x1lf, x1uf, x2lf, x2uf, x1[ii], x2[ii], &ansy[ii]);
		}

    }
    // free arrays

    for (ii=0; ii<*nx; ii++){
        free(yy[ii]);
    }
    free(yy);
}



void R_bcuint(float y[], float y1[], float y2[], float y12[], float *x1l,
    float *x1u, float *x2l, float *x2u, float *x1, float *x2, float *ansy,
    float *ansy1, float *ansy2)
{
    bcuint(y, y1, y2, y12, *x1l, *x1u, *x2l, *x2u, *x1, *x2, ansy, ansy1, ansy2);
//    Rprintf("%f\n", *ansy);
}


void R_bcuint_grid(float y[], int* nx, int* ny, float *x1l, float *x1u,
            float *x2l, float *x2u, float *x1, float *x2, int* nn, float *ansy, float *ansy1,
            float * ansy2){
    float **yy, **y1, **y2, **y12;
    float dx, dy;
    register int ii, jj;
    int idxi, idxj;
    float yf[5], y1f[5], y2f[5], y12f[5];
    float x1uf, x1lf, x2uf, x2lf;

//    for (ii=0; ii<*nn; ii++){
//        Rprintf("%f %f %f\n", x1[ii], x2[ii], ansy[ii]);
//   }
    dx = (*x1u-*x1l)/(*nx-1);
    dy = (*x2u-*x2l)/(*ny-1);

    // allocate arrays
    yy = (float**) calloc(*nx, sizeof(float*));
    y1 = (float**) calloc(*nx, sizeof(float*));
    y2 = (float**) calloc(*nx, sizeof(float*));
    y12 = (float**) calloc(*nx, sizeof(float*));

    for (ii=0; ii<*nx; ii++){
        yy[ii] = (float*) calloc(*ny, sizeof(float));
        y1[ii] = (float*) calloc(*ny, sizeof(float));
        y2[ii] = (float*) calloc(*ny, sizeof(float));
        y12[ii] = (float*) calloc(*ny, sizeof(float));
    }

    //  reconvert y to matrix
    for (jj=0; jj<*ny; jj++){
        for (ii=0; ii<*nx; ii++){
            yy[ii][jj] = y[(ii*(*ny))+jj];
//            Rprintf("%f ", yy[ii][jj]);
        }
//        Rprintf("\n");
    }
    // estimate derivatives
    //  central differences for inner grid cells
    for (jj=1; jj<(*ny-1); jj++){
        for (ii=1; ii<(*nx-1); ii++){
            y1[ii][jj] = (yy[ii+1][jj]-yy[ii-1][jj])/(2*dx);
            y2[ii][jj] = (yy[ii][jj+1]-yy[ii][jj-1])/(2*dy);
            y12[ii][jj] = (yy[ii+1][jj+1]-yy[ii-1][jj+1]-yy[ii+1][jj-1]+yy[ii-1][jj-1])/(2*dx*2*dy);
//            Rprintf("%f ", y12[ii][jj]);
        }
//        Rprintf("\n");
    }
    // one sided differences for grid border
            // sides
    for (ii=1; ii<(*nx-1); ii++){
        y1[ii][0] = (yy[ii+1][0]-yy[ii-1][0])/(2*dx);
        y1[ii][*ny-1] = (yy[ii+1][*ny-1]-yy[ii-1][*ny-1])/(2*dx);
        y2[ii][0] = (yy[ii][1]-yy[ii][0])/(dy);
        y2[ii][*ny-1] = (yy[ii][*ny-1]-yy[ii][*ny-2])/(dy);
    }
    for (jj=1; jj<(*ny-1); jj++){
        y1[0][jj] = (yy[1][jj]-yy[0][jj])/(dx);
        y1[*nx-1][jj] = (yy[*nx-1][jj]-yy[*nx-2][jj])/(dx);
        y2[0][jj] = (yy[0][jj+1]-yy[0][jj-1])/(2*dy);
        y2[*nx-1][jj] = (yy[*nx-1][jj+1]-yy[*nx-1][jj-1])/(2*dy);
    }
            // corners
    y1[0][0] = (yy[1][0]-yy[0][0])/(dx);
    y1[0][*ny-1] = (yy[1][*ny-1]-yy[0][*ny-1])/(dx);
    y1[*nx-1][0] = (yy[*nx-1][0]-yy[*nx-2][0])/(dx);
    y1[*nx-1][*ny-1] = (yy[*nx-1][*ny-1]-yy[*nx-2][*ny-1])/(dx);

    y2[0][0] = (yy[0][1]-yy[0][0])/(dy);
    y2[0][*ny-1] = (yy[0][*ny-1]-yy[0][*ny-2])/(dy);
    y2[*nx-1][0] = (yy[*nx-1][1]-yy[*nx-1][0])/(dy);
    y2[*nx-1][*ny-1] = (yy[*nx-1][*ny-1]-yy[*nx-1][*ny-2])/(dy);

    //  cross derivative
    for (ii=1; ii<(*nx-1); ii++){
        y12[ii][0] = (y1[ii][1]-y1[ii][0])/(dy);
        y12[ii][*ny-1] = (y1[ii][*ny-1]-y1[ii][*ny-2])/(dy);
    }
    for (jj=1; jj<(*ny-1); jj++){
        y12[0][jj] = (y1[0][jj+1]-y1[0][jj-1])/(2*dy);
        y12[*nx-1][jj] = (y1[*nx-1][jj+1]-y1[*nx-1][jj-1])/(2*dy);
    }
    y12[0][0] = (y1[0][1]-y1[0][0])/(dy);
    y12[0][*ny-1] = (y1[0][*ny-1]-y1[0][*ny-2])/(dy);
    y12[*nx-1][0] = (y1[*nx-1][1]-y1[*nx-1][0])/(dy);
    y12[*nx-1][*ny-1] = (y1[*nx-1][*ny-1]-y1[*nx-1][*ny-2])/(dy);

    /*
    Rprintf("\ny1\n");
    for (jj=0; jj<*ny; jj++){
        for (ii=0; ii<*nx; ii++){
            Rprintf("%f ", y1[ii][jj]);
        }
        Rprintf("\n");
    }
    Rprintf("\ny2\n");
    for (jj=0; jj<*ny; jj++){
        for (ii=0; ii<*nx; ii++){
            Rprintf("%f ", y2[ii][jj]);
        }
        Rprintf("\n");
    }
    Rprintf("\ny12\n");
    for (jj=0; jj<*ny; jj++){
        for (ii=0; ii<*nx; ii++){
            Rprintf("%f ", y12[ii][jj]);
        }
        Rprintf("\n");
    }
    */

    // estimate neighbouring grid cells
    for(ii=0; ii<*nn; ii++){
        idxi = (x1[ii] - *x1l)/dx;
        idxj = (x2[ii] - *x2l)/dy;
        // make sure to stay within grid
        if (idxi==(*nx-1))  idxi = idxi-1;
        if (idxj==(*ny-1))  idxj = idxj-1;

        if (idxi>(*nx-1) || idxj>(*ny-1) || idxi<0 || idxj<0) {
            ansy[ii] = log(-1) ;
//            Rprintf("%d\n", ii);
            continue ;
        }

//        Rprintf("%f %d %f %d\n", x1[ii], idxi , x2[ii], idxj);

    // construct input for bcuint
        yf[1] = yy[idxi][idxj];
        yf[2] = yy[idxi+1][idxj];
        yf[3] = yy[idxi+1][idxj+1];
        yf[4] = yy[idxi][idxj+1];

        y1f[1] = y1[idxi][idxj];
        y1f[2] = y1[idxi+1][idxj];
        y1f[3] = y1[idxi+1][idxj+1];
        y1f[4] = y1[idxi][idxj+1];

        y2f[1] = y2[idxi][idxj];
        y2f[2] = y2[idxi+1][idxj];
        y2f[3] = y2[idxi+1][idxj+1];
        y2f[4] = y2[idxi][idxj+1];

        y12f[1] = y12[idxi][idxj];
        y12f[2] = y12[idxi+1][idxj];
        y12f[3] = y12[idxi+1][idxj+1];
        y12f[4] = y12[idxi][idxj+1];

        x1uf = *x1l + dx*(idxi+1);
        x1lf = *x1l + dx*(idxi);
        x2uf = *x2l + dy*(idxj+1);
        x2lf = *x2l + dy*(idxj);

    // bicubic interpolation
//        Rprintf("%f %f\n", x1[ii], x2[ii]);
        bcuint(yf, y1f, y2f, y12f, x1lf, x1uf, x2lf, x2uf, x1[ii], x2[ii], &ansy[ii], ansy1, ansy2);

    }
    // free arrays

    for (ii=0; ii<*nx; ii++){
        free(yy[ii]);
        free(y1[ii]);
        free(y2[ii]);
        free(y12[ii]);
    }
    free(yy);
    free(y1);
    free(y2);
    free(y12);
}
