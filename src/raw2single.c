void raw2single(unsigned char* raw, int* nn, float* fl){
    float* tmp;
    int i;
    
    for (i=0; i<*nn; i++){
    
        tmp = (float*) &(raw[i*4]) ; 
        fl[i] = (float) *tmp;
    }
        
}
