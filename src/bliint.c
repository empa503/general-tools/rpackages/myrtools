#define NRANSI
#include "nrutil.h"

void bliint(double y[], double x1l,
    double x1u, double x2l, double x2u, double x1, double x2, double *ansy)
{
    double t,u;

    t = (x1-x1l)/(x1u-x1l);
    u = (x2-x2l)/(x2u-x2l);

//    Rprintf("%f\n", t);
//    Rprintf("%f\n", u);

    *ansy = (1-t)*(1-u)*y[1] + t*(1-u)*y[2] + t*u*y[3] + u*(1-t)*y[4];

    return;
}

#undef NRANSI
