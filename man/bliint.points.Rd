\name{bliint.points}
\alias{bliint.points}
\title{ bilinear interpolation from a grid to points }
\description{
    Given values on a regular grid (2D array), values are interpolated using bilinear interpolation to points. 
}
\usage{
bliint.points(z, xrng, yrng, x, y, flip = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{z}{ (2D, numeric) matrix containing the original values }
  \item{xrng}{ (2 element numeric) the range of matrix z in x-direction }
  \item{yrng}{ (2 element numeric) the range of matrix z in y-direction }
  \item{x}{ (numeric) x position of points where to interpolate }
  \item{y}{ (numeric) y position of points where to interpolate }
  \item{flip}{ (logical) flip the y-orientation of input matrix (necessary if y-direction of matrix is decreasing), default is TRUE }
}
\details{

}
\value{
    (numeric vector) of interpolated values, for points outside the grid domain NaN is returned
}
\references{ Numerical Recipes, Press 1992 }
\author{ stephan.henne@empa.ch }
\seealso{ \code{\link{bliint.grid}} }
\examples{
    fy = function(x1, x2) return(x1+x2)
    
    x1l = 0
    x1u = 1
    x2l = 0
    x2u = 1

    nx = 40
    ny = 30

    xx1 = matrix(rep(seq(x1l, x1u, length.out=nx), each=ny), nrow=ny) 
    xx2 = matrix(rep(seq(x2l, x2u, length.out=ny), nx), nrow=ny) 
    xx2 = xx2[nrow(xx2):1,]
    y = fy(xx1, xx2)
    
    x1 = c(0.2, -0.2)
    x2 = c(0.2, 0.2)
    y.int = bliint.points(y, c(x1l, x1u), c(x2l, x2u), x1, x2, flip=TRUE)
    print(y.int)
    print(fy(x1, x2))

}
\keyword{ manip }

