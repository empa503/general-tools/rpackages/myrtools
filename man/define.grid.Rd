\name{define.grid}
\alias{define.grid}
\title{
	define grid points in projection
}
\description{
	For a given grid projection coordinates of grid centres are calculated in lat/lon coordinates.  
}
\usage{
define.grid(type = 4, x0 = 0, y0 = 90, dx = 50, dy = 50, nx = 101, ny = 101)
}
\arguments{
  \item{type}{ (integer) type of grid, see \code{\link{setPROJ}} for details, so far only type=4 (stereographic projection) is supported }
  \item{x0}{ grid center longitude}
  \item{y0}{ grid center latitude }
  \item{dx}{ grid distance in longitude direction (km) }
  \item{dy}{ grid distance in latitude direction (km) }
  \item{nx}{ number of grid points in longitude direction }
  \item{ny}{ number of grid points in latitude direction }
}
\details{

}
\value{
lat=lat, lng=lng, xrng=xrng, yrng=yrng, dx=dx, dy=dy, A=aa, projection=projection, parameters=parameters, orientation=orientation)%%  ~Describe the value returned
List 
	\item{lat}{latitudes of grid point centres }
	\item{lng}{longitudes of grid point centres }
	\item{xrng}{longitudes extent of grid point centres }
	\item{yrng}{latitudes extent of grid point centres }
	\item{dx}{ resolution in longitude direction (km) }
	\item{dy}{ resolution in latitude direction (km) }
	\item{A}{ surface area of grid boxes (km^2) }
	\item{projection}{ name of projection as it is used by mapproject }
	\item{parameters}{ list of projection parameters as used by mapproject }
	\item{orientation}{ orientation of projection as used by mapproject }				 	
}
\references{
}
\author{
	stephan.henne@empa.ch
}
\note{
}

\seealso{
	\code{\link{mapproject}}
}
\examples{
nx = 101
ny = 51
dx = 30
dy = 60
proj.type=4

grid.def = define.grid(type=proj.type, x0=10, y0=45, dx=dx, dy=dy, nx=nx, ny=ny)

########################## plot grid centers on a regular map
map(xlim=range(grid.def$lng), ylim=range(grid.def$lat))
points(grid.def$lng, grid.def$lat, pch=".")

########################## plot grid centers on the projected map
mp = mapproject(as.vector(grid.def$lng), as.vector(grid.def$lat), projection=grid.def$projection, parameters=grid.def$parameters, orientation=grid.def$orientation)
plot.new()
plot.window(range(mp$x, na.rm=TRUE), range(mp$y, na.rm=TRUE), xaxs="i", yaxs="i")
map(add=TRUE, projection=grid.def$projection, parameters=grid.def$parameters, orientation=grid.def$orientation, xlim=range(grid.def$lng, na.rm=TRUE), ylim=range(grid.def$lat, na.rm=TRUE))
points(mp$x, mp$y, pch=".")

########################## plot filled grid on projection 
aa = list(xrng = grid.def$xrng, yrng = grid.def$yrng, zz=matrix(rnorm(nx*ny), ncol=nx), projection=grid.def$projection, parameters=grid.def$parameters, orientation=grid.def$orientation)
fill.grid(aa, borders=TRUE)
	
}
\keyword{ manip }
