\name{divide.grid}
\alias{divide.grid}
\alias{grid.refine}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Refine 2D regular grid by evenly dividing grid cells
}
\description{
Each grid cell of a 2D regular grid is divided into div x div subgrid cells. A new grid structure is returned containing updated grid extensions. 
}
\usage{
divide.grid(zz, xrng, yrng, div = 2, dens.cor = TRUE, flipped = TRUE)
grid.refine(zz, xrng, yrng, div = 2, dens.cor = TRUE, flipped = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{zz}{
	2D array
}
  \item{xrng}{
	2-element vector (numeric)	x-range of array
}
  \item{yrng}{
	2-element vector (numeric)	y-range of array
}
  \item{div}{
	(integer)	Number of new subgrids in x and y direction. Each grid cell is divided into div x div subgrid cells
}
  \item{dens.cor}{
	(logical)	If FALSE values in original grid are simply copied to subgrid cells, if TRUE the values in the subgrid cells are divided by the number of subgrid cells in each main grid cell.
}
  \item{flipped}{
	(logical)	If TRUE y order of array is reversed.
}
}
\value{
	New grid representation with refined grid structure.
  \item{xrng}{grid extent in x direction}
  \item{yrng}{grid extent in y direction}
}

\author{
	stephan.henne@empa.ch
}

\seealso{
 See also \code{\link{pool.grid}}, \code{\link{extend.grid}}
}
\examples{

require(myRplots)
org = list(xrng = c(0,10), yrng = c(0,10), zz = array(NA, c(11,11)))
org$zz[,] = 1
org$zz[5,2] = 2
org$zz[3,1] = 3
org$zz[10,9] = 4

fill.grid(org, rotate=FALSE)
div4 = divide.grid(org$zz, org$xrng, org$yrng, div=4, flipped=FALSE, dens.cor=TRUE)
fill.grid(div4, border=TRUE, rotate=FALSE)

}

\keyword{ manip }
