\name{time.series.review}
\alias{time.series.review}
\title{
	Interactive reviewing of time series
}
\description{
	The function allows interactive reviewing of a time series. This includes mainly displaying the data 
of in an x-y fashion, comparing differnt parameters, zooming, flagging data and plot export. 
}
\usage{
time.series.review(X, cpd, dtm.col = "dtm", plot.lines = FALSE, show.all.cpd = FALSE)
}
\arguments{
  \item{X}{(data.frame) Containing different parameters 'cpd' that will be displayed on the y-axis and one parameter 'dtm.col' that serves as x-axis (typically the time axis). The data frame may contain columns that serve as flag information. These have to contain the suffix 'flag' in their column name.}
  \item{cpd}{(characer, vector) Names of parameters of X to be analysed. The function loops through these parameters and allows flagging each data series individually.}
  \item{dtm.col}{(character) Name of x-axis parameter of X.}
  \item{plot.lines}{(logical) The default display of the data is points of the active parameter only. If plot.lines is set to TRUE, data are additionally connected by lines.}
  \item{show.all.cpd}{(logical) The default display only shows the data of the active parameter. If show.all.cpd is set to TRUE, all parameters are displayed as lines.}
}
\details{
	The functions first ask for the parameters that should be reviewed, even if they were given in cpd. Then the reviewing of each parameter is done individually. Throughout the excution the user interacts through select.list and dialog string interfaces. In addtion, the locator and identify functions are made use of for interactive zooming and selection of individual points. 

Flag values have to be numerics! A default flag value of 0 is used if no flag information was passed in the input data.frame.
}
\value{Returns a data.frame with the same number of rows as the input data.frame, containing the same data as the latter, possibly supplemented by additional flag columns that were not available in the original data.}
\author{ stephan.henne@empa.ch }

\seealso{ \code{\link{time.series.review.plot}}, \code{\link{time.series.flag}}}
\examples{
dtm = seq(0,365,1)
dat = data.frame(dtm=dtm, "co"=rnorm(length(dtm)), "m1"=rnorm(length(dtm)))
dat$m2 = dat$m1*1.1

dat.mod = time.series.review(dat, plot.lines=TRUE, show.all.cpd=TRUE)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ manip }
