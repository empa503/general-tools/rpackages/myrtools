\name{get.field.value}
\alias{get.field.value}
\title{ getting value on a grid using bilinear interpolation }
\description{
    bi-linear interpolation on a grid
}
\usage{
get.field.value(topo, xx, yy, verbose=FALSE)
}
\arguments{
  \item{topo}{ (list) containing xrng, yrng, and zz }
  \item{xx}{ (numeric) x position of where value should be interpolated }
  \item{yy}{ (numeric) y position of where value should be interpolated }
  \item{verbose}{ (logical) should additional output be written }
}
\details{
-
}
\value{
    (numeric) value at postion xx, yy
}
\author{ Stephan Henne <stephan.henne@empa.ch> }
\keyword{ manip }
